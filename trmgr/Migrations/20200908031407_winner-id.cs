﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace trmgr.Migrations
{
    public partial class winnerid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LooserId",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LooserRegistrationId",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WinnerId",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WinnerRegistrationId",
                table: "Matches",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Matches_LooserRegistrationId",
                table: "Matches",
                column: "LooserRegistrationId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_WinnerRegistrationId",
                table: "Matches",
                column: "WinnerRegistrationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_Registrations_LooserRegistrationId",
                table: "Matches",
                column: "LooserRegistrationId",
                principalTable: "Registrations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_Registrations_WinnerRegistrationId",
                table: "Matches",
                column: "WinnerRegistrationId",
                principalTable: "Registrations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Registrations_LooserRegistrationId",
                table: "Matches");

            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Registrations_WinnerRegistrationId",
                table: "Matches");

            migrationBuilder.DropIndex(
                name: "IX_Matches_LooserRegistrationId",
                table: "Matches");

            migrationBuilder.DropIndex(
                name: "IX_Matches_WinnerRegistrationId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "LooserId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "LooserRegistrationId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "WinnerId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "WinnerRegistrationId",
                table: "Matches");
        }
    }
}

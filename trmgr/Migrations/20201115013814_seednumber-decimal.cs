﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace trmgr.Migrations
{
    public partial class seednumberdecimal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "SeedNumber",
                table: "Matches",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "SeedNumber",
                table: "Matches",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}

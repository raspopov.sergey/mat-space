﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace trmgr.Migrations
{
    public partial class clubnameconstraint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ClubNames",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "ClubNames",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}

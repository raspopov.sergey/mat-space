﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace trmgr.Migrations
{
    public partial class matchregs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matches_MatchCompetitor_Competitor1Id",
                table: "Matches");

            migrationBuilder.DropForeignKey(
                name: "FK_Matches_MatchCompetitor_Competitor2Id",
                table: "Matches");

            migrationBuilder.DropTable(
                name: "MatchCompetitor");

            migrationBuilder.RenameColumn(
                name: "Competitor2Id",
                table: "Matches",
                newName: "Registration2Id");

            migrationBuilder.RenameColumn(
                name: "Competitor1Id",
                table: "Matches",
                newName: "Registration1Id");

            migrationBuilder.RenameIndex(
                name: "IX_Matches_Competitor2Id",
                table: "Matches",
                newName: "IX_Matches_Registration2Id");

            migrationBuilder.RenameIndex(
                name: "IX_Matches_Competitor1Id",
                table: "Matches",
                newName: "IX_Matches_Registration1Id");

            migrationBuilder.AddColumn<byte>(
                name: "Advantages1",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Advantages2",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<bool>(
                name: "IsDQed1",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDQed2",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte>(
                name: "Penalties1",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Penalties2",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Score1",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "Score2",
                table: "Matches",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_Registrations_Registration1Id",
                table: "Matches",
                column: "Registration1Id",
                principalTable: "Registrations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_Registrations_Registration2Id",
                table: "Matches",
                column: "Registration2Id",
                principalTable: "Registrations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Registrations_Registration1Id",
                table: "Matches");

            migrationBuilder.DropForeignKey(
                name: "FK_Matches_Registrations_Registration2Id",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Advantages1",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Advantages2",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsDQed1",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsDQed2",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Penalties1",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Penalties2",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Score1",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "Score2",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "Registration2Id",
                table: "Matches",
                newName: "Competitor2Id");

            migrationBuilder.RenameColumn(
                name: "Registration1Id",
                table: "Matches",
                newName: "Competitor1Id");

            migrationBuilder.RenameIndex(
                name: "IX_Matches_Registration2Id",
                table: "Matches",
                newName: "IX_Matches_Competitor2Id");

            migrationBuilder.RenameIndex(
                name: "IX_Matches_Registration1Id",
                table: "Matches",
                newName: "IX_Matches_Competitor1Id");

            migrationBuilder.CreateTable(
                name: "MatchCompetitor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityAlwaysColumn),
                    Advantages = table.Column<byte>(nullable: false),
                    IsDQed = table.Column<bool>(nullable: false),
                    Penalties = table.Column<byte>(nullable: false),
                    RegistrationId = table.Column<int>(nullable: false),
                    Score = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchCompetitor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MatchCompetitor_Registrations_RegistrationId",
                        column: x => x.RegistrationId,
                        principalTable: "Registrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MatchCompetitor_RegistrationId",
                table: "MatchCompetitor",
                column: "RegistrationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_MatchCompetitor_Competitor1Id",
                table: "Matches",
                column: "Competitor1Id",
                principalTable: "MatchCompetitor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Matches_MatchCompetitor_Competitor2Id",
                table: "Matches",
                column: "Competitor2Id",
                principalTable: "MatchCompetitor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;

namespace trmgr.DAL.Seeding
{
    public class CategoriesIbjjf
    {
        private const string AGE_ADULT = "IBJJF Adult";
        private const string AGE_JUNIOR = "IBJJF Junior";
        private const string AGE_JUVENILE = "IBJJF Juvenile";
        private const string EXP_ADULT = "IBJJF Adult Belts";
        private const string EXP_JUNIOR = "IBJJF Junior Belts";
        private const string GENDER = "Standart";
        private const string WEIGHT_ADULT_MALE = "IBJJF Adult Male";
        private const string WEIGHT_ADULT_FEMALE = "IBJJF Adult Female";
        private const string WEIGHT_JUVENILE_MALE = "IBJJF Juvenile Male";
        private const string WEIGHT_JUVENILE_FEMALE = "IBJJF Juvenile Female";

        private ApplicationDbContext _context;

        public CategoriesIbjjf(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task PopulateCategories(string userId)
        {
            if (await _context.AgeCategoryGroups.FirstOrDefaultAsync(g => g.Name == AGE_ADULT) == null)
                await AddAdultAgeCategories(userId);
            if (await _context.AgeCategoryGroups.FirstOrDefaultAsync(g => g.Name == AGE_JUNIOR) == null)
                await AddJuniorAgeCategories(userId);
            if (await _context.AgeCategoryGroups.FirstOrDefaultAsync(g => g.Name == AGE_JUVENILE) == null)
                await AddJuvenileAgeCategories(userId);

            if (await _context.ExperienceCategoryGroups.FirstOrDefaultAsync(g => g.Name == EXP_ADULT) == null)
                await AddAdultExperienceCategories(userId);
            if (await _context.ExperienceCategoryGroups.FirstOrDefaultAsync(g => g.Name == EXP_JUNIOR) == null)
                await AddJuniorExperienceCategories(userId);

            if (await _context.GenderCategoryGroups.FirstOrDefaultAsync(g => g.Name == GENDER) == null)
                await AddGenderCategories(userId);

            if (await _context.WeightCategoryGroups.FirstOrDefaultAsync(g => g.Name == WEIGHT_ADULT_MALE) == null)
                await AddAdultMaleWeightCategories(userId);
            if (await _context.WeightCategoryGroups.FirstOrDefaultAsync(g => g.Name == WEIGHT_ADULT_FEMALE) == null)
                await AddAdultFemaleWeightCategories(userId);
            if (await _context.WeightCategoryGroups.FirstOrDefaultAsync(g => g.Name == WEIGHT_JUVENILE_MALE) == null)
                await AddJuvenileMaleWeightCategories(userId);
            if (await _context.WeightCategoryGroups.FirstOrDefaultAsync(g => g.Name == WEIGHT_JUVENILE_FEMALE) == null)
                await AddJuvenileFemaleWeightCategories(userId);
        }

        public async Task AddAdultAgeCategories(string userId)
        {
            var group = new AgeCategoryGroup() { Name = AGE_ADULT, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new AgeCategory(){Name = "Adult", MinAge = 18, MaxAge = 30 },
                new AgeCategory(){Name = "Master 1", MinAge = 30, MaxAge = 36 },
                new AgeCategory(){Name = "Master 2", MinAge = 36, MaxAge = 41 },
                new AgeCategory(){Name = "Master 3", MinAge = 41, MaxAge = 46 },
                new AgeCategory(){Name = "Master 4", MinAge = 46, MaxAge = 51 },
                new AgeCategory(){Name = "Master 5", MinAge = 51, MaxAge = 56 },
                new AgeCategory(){Name = "Master 6", MinAge = 56, MaxAge = 0 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddJuniorAgeCategories(string userId)
        {
            var group = new AgeCategoryGroup() { Name = AGE_JUNIOR, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new AgeCategory(){Name = "Mighty Mite 1", MinAge = 4, MaxAge = 5 },
                new AgeCategory(){Name = "Mighty Mite 2", MinAge = 5, MaxAge = 6 },
                new AgeCategory(){Name = "Mighty Mite 3", MinAge = 6, MaxAge = 7 },
                new AgeCategory(){Name = "Pee Wee 1", MinAge = 7, MaxAge = 8 },
                new AgeCategory(){Name = "Pee Wee 2", MinAge = 8, MaxAge = 9 },
                new AgeCategory(){Name = "Pee Wee 3", MinAge = 9, MaxAge = 10 },
                new AgeCategory(){Name = "Junior 1", MinAge = 10, MaxAge = 11 },
                new AgeCategory(){Name = "Junior 2", MinAge = 11, MaxAge = 12 },
                new AgeCategory(){Name = "Junior 3", MinAge = 12, MaxAge = 13 },
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddJuvenileAgeCategories(string userId)
        {
            var group = new AgeCategoryGroup() { Name = AGE_JUVENILE, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new AgeCategory(){Name = "Teen 1", MinAge = 13, MaxAge = 14 },
                new AgeCategory(){Name = "Teen 2", MinAge = 14, MaxAge = 15 },
                new AgeCategory(){Name = "Teen 3", MinAge = 15, MaxAge = 16 },
                new AgeCategory(){Name = "Juvenile 1", MinAge = 16, MaxAge = 17 },
                new AgeCategory(){Name = "Juvenile 2", MinAge = 17, MaxAge = 18 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }


        public async Task AddAdultExperienceCategories(string userId)
        {
            var group = new ExperienceCategoryGroup() { Name = EXP_ADULT, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new ExperienceCategory(){ Name = "White", Difficulty = 1 },
                new ExperienceCategory(){ Name = "Blue", Difficulty = 2 },
                new ExperienceCategory(){ Name = "Purple", Difficulty = 3 },
                new ExperienceCategory(){ Name = "Brown", Difficulty = 4 },
                new ExperienceCategory(){ Name = "Black", Difficulty = 5 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddJuniorExperienceCategories(string userId)
        {
            var group = new ExperienceCategoryGroup() { Name = EXP_JUNIOR, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new ExperienceCategory(){ Name = "White", Difficulty = 1 },
                new ExperienceCategory(){ Name = "Gray", Difficulty = 2 },
                new ExperienceCategory(){ Name = "Yellow", Difficulty = 3 },
                new ExperienceCategory(){ Name = "Orange", Difficulty = 4 },
                new ExperienceCategory(){ Name = "Green", Difficulty = 5 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }


        public async Task AddGenderCategories(string userId)
        {
            var group = new GenderCategoryGroup() { Name = GENDER, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new GenderCategory(){ Name = "Male" },
                new GenderCategory(){ Name = "Female" }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }


        public async Task AddAdultMaleWeightCategories(string userId)
        {
            var group = new WeightCategoryGroup() { Name = WEIGHT_ADULT_MALE, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new WeightCategory(){ Name = "Rooster", MinWeight = 0, MaxWeight = 127 },
                new WeightCategory(){ Name = "Light Feather", MinWeight = 127, MaxWeight = 141.5m },
                new WeightCategory(){ Name = "Feather", MinWeight = 141.5m, MaxWeight = 154.5m },
                new WeightCategory(){ Name = "Light", MinWeight = 154.5m, MaxWeight = 168 },
                new WeightCategory(){ Name = "Middle", MinWeight = 168, MaxWeight = 181.5m },
                new WeightCategory(){ Name = "Medium Heavy", MinWeight = 181.5m, MaxWeight = 195 },
                new WeightCategory(){ Name = "Heavy", MinWeight = 195, MaxWeight = 208 },
                new WeightCategory(){ Name = "Super Heavy", MinWeight = 208, MaxWeight = 222 },
                new WeightCategory(){ Name = "Ultra Heavy", MinWeight = 222, MaxWeight = 0 },
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddJuvenileMaleWeightCategories(string userId)
        {
            var group = new WeightCategoryGroup() { Name = WEIGHT_JUVENILE_MALE, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new WeightCategory(){ Name = "Rooster", MinWeight = 0, MaxWeight = 118 },
                new WeightCategory(){ Name = "Light Feather", MinWeight = 118, MaxWeight = 129 },
                new WeightCategory(){ Name = "Feather", MinWeight = 129, MaxWeight = 141 },
                new WeightCategory(){ Name = "Light", MinWeight = 141, MaxWeight = 152 },
                new WeightCategory(){ Name = "Middle", MinWeight = 152, MaxWeight = 163 },
                new WeightCategory(){ Name = "Medium Heavy", MinWeight = 163, MaxWeight = 174.5m },
                new WeightCategory(){ Name = "Heavy", MinWeight = 174.5m, MaxWeight = 185.5m },
                new WeightCategory(){ Name = "Super Heavy", MinWeight = 185.5m, MaxWeight = 196.5m },
                new WeightCategory(){ Name = "Ultra Heavy", MinWeight = 196.5m, MaxWeight = 0 },
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddAdultFemaleWeightCategories(string userId)
        {
            var group = new WeightCategoryGroup() { Name = WEIGHT_ADULT_FEMALE, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new WeightCategory(){ Name = "Light Feather", MinWeight = 0, MaxWeight = 118m },
                new WeightCategory(){ Name = "Feather", MinWeight = 118m, MaxWeight = 129m },
                new WeightCategory(){ Name = "Light", MinWeight = 129, MaxWeight = 141 },
                new WeightCategory(){ Name = "Middle", MinWeight = 141, MaxWeight = 152m },
                new WeightCategory(){ Name = "Medium Heavy", MinWeight = 152, MaxWeight = 163 },
                new WeightCategory(){ Name = "Heavy", MinWeight = 163, MaxWeight = 0 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }

        public async Task AddJuvenileFemaleWeightCategories(string userId)
        {
            var group = new WeightCategoryGroup() { Name = WEIGHT_JUVENILE_FEMALE, ApplicationUserId = userId };
            var categories = new List<Category>()
            {
                new WeightCategory(){ Name = "Light Feather", MinWeight = 0, MaxWeight = 106.5m },
                new WeightCategory(){ Name = "Feather", MinWeight = 106.5m, MaxWeight = 116 },
                new WeightCategory(){ Name = "Light", MinWeight = 116, MaxWeight = 125 },
                new WeightCategory(){ Name = "Middle", MinWeight = 125, MaxWeight = 133.5m },
                new WeightCategory(){ Name = "Medium Heavy", MinWeight = 133.5m, MaxWeight = 144 },
                new WeightCategory(){ Name = "Heavy", MinWeight = 114, MaxWeight = 0 }
            };
            group.Categories = categories;
            _context.Add(group);
            await _context.SaveChangesAsync();
        }
    }

}

﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using trmgr.Models.DatabaseModels;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;
using trmgr.Models.DatabaseModels.School;

namespace trmgr.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Country> Countries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<Affiliation> Affiliations { get; set; }

        public DbSet<AgeCategoryGroup> AgeCategoryGroups { get; set; }
        public DbSet<AgeCategory> AgeCategories { get; set; }
        public DbSet<ExperienceCategoryGroup> ExperienceCategoryGroups { get; set; }
        public DbSet<ExperienceCategory> ExperienceCategories { get; set; }
        public DbSet<GenderCategoryGroup> GenderCategoryGroups { get; set; }
        public DbSet<GenderCategory> GenderCaegories { get; set; }
        public DbSet<WeightCategoryGroup> WeightCategoryGroups { get; set; }
        public DbSet<WeightCategory> WeightCategories { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<Bracket> Brackets { get; set; }

        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<Division> Divisions { get; set; }
        public DbSet<DivisionGroup> DivisionGroups { get; set; }

        public DbSet<AgeCategoryDivisionGroup> AgeCategoryDivisionGroups { get; set; }
        public DbSet<ExperienceCategoryDivisionGroup> ExperienceCategoryDivisionGroups { get; set; }
        public DbSet<GenderCategoryDivisionGroup> GenderCategoryDivisionGroups { get; set; }
        public DbSet<WeightCategoryDivisionGroup> WeightCategoryDivisionGroups { get; set; }

        public DbSet<Registration> Registrations { get; set; }
        public DbSet<RegisteredDivision> RegisteredDivisions { get; set; }
        public DbSet<ClubName> ClubNames { get; set; }


        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ForNpgsqlUseIdentityAlwaysColumns();

            builder.Entity<CategoryDivisionGroup>()
                .HasKey(cdg => new { cdg.CategoryId, cdg.DivisionGroupId });

            builder.Entity<CategoryDivisionGroup>().HasOne(cdg => cdg.Category)
                .WithMany(c => c.DivisionGroups).HasForeignKey(cdg => cdg.CategoryId);

            builder.Entity<AgeCategoryDivisionGroup>().HasOne(cdg => cdg.DivisionGroup)
                .WithMany(c => c.AgeCategories).HasForeignKey(cdg => cdg.DivisionGroupId);
            builder.Entity<ExperienceCategoryDivisionGroup>().HasOne(cdg => cdg.DivisionGroup)
                .WithMany(c => c.ExperienceCategories).HasForeignKey(cdg => cdg.DivisionGroupId);
            builder.Entity<GenderCategoryDivisionGroup>().HasOne(cdg => cdg.DivisionGroup)
                .WithMany(c => c.GenderCategories).HasForeignKey(cdg => cdg.DivisionGroupId);
            builder.Entity<WeightCategoryDivisionGroup>().HasOne(cdg => cdg.DivisionGroup)
                .WithMany(c => c.WeightCategories).HasForeignKey(cdg => cdg.DivisionGroupId);

            builder.Entity<Registration>().HasAlternateKey(r => new { r.TournamentId, r.EmailAddress }).HasName("U_Registration");
            builder.Entity<RegisteredDivision>().HasOne(rd => rd.AgeCategory).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<RegisteredDivision>().HasOne(rd => rd.GenderCategory).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<RegisteredDivision>().HasOne(rd => rd.ExperienceCategory).WithMany().OnDelete(DeleteBehavior.Restrict);
            builder.Entity<RegisteredDivision>().HasOne(rd => rd.WeightCategory).WithMany().OnDelete(DeleteBehavior.Restrict);
        }
    }
}

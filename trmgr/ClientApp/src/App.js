﻿import React from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators from './store/Auth/actionCreators';

import MainNavBar from './components/Navigation/MainNavBar';
import MainHeader from './components/Navigation/MainHeader';
import HeaderWrapper from './components/Navigation/HeaderWrapper';

import SignIn from './containers/Auth/SignIn';
import SignUp from './containers/Auth/SignUp';
import UserProfile from './containers/User/UserProfile';

import OrganizerDashboard from './containers/Organizer/OrganizerDashboard';
import AgeCategories from './containers/Organizer/Categories/AgeCategories';
import ExperienceCategories from './containers/Organizer/Categories/ExperienceCategories';
import GenderCategories from './containers/Organizer/Categories/GenderCategories';
import WeightCategories from './containers/Organizer/Categories/WeightCategories';
import DivisionAges from './containers/Organizer/DivisionGroup/Ages';
import DivisionGenders from './containers/Organizer/DivisionGroup/Genders';
import DivisionExperiences from './containers/Organizer/DivisionGroup/Experiences';
import DivisionWeights from './containers/Organizer/DivisionGroup/Weights';
import Tournament from './containers/Organizer/Tournament';

import Tournaments from './containers/Tournaments';
import Registration from './containers/Public/Registration';
import Registrations from './containers/Public/Registrations';
import Bracket from './containers/Public/Bracket';


class App extends React.Component {
    state = {
        menuVisible: false
    }

    componentWillMount() {
        //this.props.initAuth(); moved into render
    }

    handleToggleMenu = () => {
        this.setState({ menuVisible: !this.state.menuVisible });
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'));
        const exp = +localStorage.getItem('exp');
        if (user)
            if (exp < Date.now()) 
                user = null;
        
        
        if (user && user.roles && user.roles.length > 0) {
            if (user.roles.includes('Organizer')) {
                const navigationItems = [
                    { path: "/organizer-dashboard", text: "Home" },
                    { path: "/age-categories", text: "Ages" },
                    { path: "/experience-categories", text: "Levels" },
                    { path: "/gender-categories", text: "Genders" },
                    { path: "/weight-categories", text: "Weights" }
                ];

                return (
                    <div>
                        <MainHeader onMenuOpen={this.handleToggleMenu} onSignOut={this.props.signOut} isSignedIn={user !== null} />
                        <MainNavBar items={navigationItems} visible={this.state.menuVisible} onClose={this.handleToggleMenu} />
                        
                        <div>
                            <Switch>
                                <Route path="/organizer-dashboard" component={OrganizerDashboard} />
                                <Route path="/age-categories" component={AgeCategories} />
                                <Route path="/experience-categories" component={ExperienceCategories} />
                                <Route path="/gender-categories" component={GenderCategories} />
                                <Route path="/weight-categories" component={WeightCategories} />
                                <Route path="/division-groups/:id/ages" component={DivisionAges} />
                                <Route path="/division-groups/:id/genders" component={DivisionGenders} />
                                <Route path="/division-groups/:id/experiences" component={DivisionExperiences} />
                                <Route path="/division-groups/:id/weights" component={DivisionWeights} />
                                <Route path="/division-group/:id" component={Registrations} />
                                <Route path="/tournament/:id" component={Tournament} />
                                <Route path="/brackets/:id" component={Bracket} />
                                <Redirect to="/organizer-dashboard" />
                            </Switch>
                        </div>
                    </div>
                    
                );
            }
            else if (user.roles.includes('Competitor')) {
                return (
                    <Switch>
                        <Route path="/user-profile" component={UserProfile} />
                        <Redirect to="/user-profile" />
                    </Switch>
                );
            }
        }
        else {
            return (
                <HeaderWrapper>
                    <Switch>
                        <Route path="/signin" component={SignIn} />
                        <Route path="/signup" component={SignUp} />
                        <Route path="/tournaments" component={Tournaments} />
                        <Route path="/register/:id" component={Registration} />
                        <Redirect to="/tournaments" />
                    </Switch>
                </HeaderWrapper>
            );
        }
    }
}

export default withRouter(connect(
    state => state.auth,
    dispatch => bindActionCreators(actionCreators, dispatch))(App));

﻿//=========================================== get category groups
export const REQUEST_CATEGORIES = 'REQUEST_CATEGORIES';
export const RECEIVE_CATEGORIES = 'RECEIVE_CATEGORIES';
export const RECEIVE_CATEGORIES_ERROR = 'RECEIVE_CATEGORIES_ERROR';

//===================================  add group
export const REQUEST_ADD_CATEGORY_GROUP = 'REQUEST_ADD_CATEGORY_GROUP';
export const RECEIVE_ADD_CATEGORY_GROUP = 'RECEIVE_ADD_CATEGORY_GROUP';
export const ERROR_ADD_CATEGORY_GROUP = 'ERROR_ADD_CATEGORY_GROUP';

//================================= add category
export const REQUEST_ADD_CATEGORY = 'REQUEST_ADD_CATEGORY';
export const RECEIVE_ADD_CATEGORY = 'RECEIVE_ADD_CATEGORY';
export const ERROR_ADD_CATEGORY = 'ERROR_ADD_CATEGORY';

//================================ update group
export const REQUEST_UPDATE_CATEGORY_GROUP = 'REQUEST_UPDATE_CATEGORY_GROUP';
export const RECEIVE_UPDATE_CATEGORY_GROUP = 'RECEIVE_UPDATE_CATEGORY_GROUP';
export const ERROR_UPDATE_CATEGORY_GROUP = 'ERROR_UPDATE_CATEGORY_GROUP';

//=============================== delete group
export const REQUEST_DELETE_CATEGORY_GROUP = 'REQUEST_DELETE_CATEGORY_GROUP';
export const RECEIVE_DELETE_CATEGORY_GROUP = 'RECEIVE_DELETE_CATEGORY_GROUP';
export const ERROR_DELETE_CATEGORY_GROUP = 'ERROR_DELETE_CATEGORY_GROUP';

//=============================== update category
export const REQUEST_UPDATE_CATEGORY = 'REQUEST_UPDATE_CATEGORY';
export const RECEIVE_UPDATE_CATEGORY = 'RECEIVE_UPDATE_CATEGORY';
export const ERROR_UPDATE_CATEGORY = 'ERROR_UPDATE_CATEGORY';

//=============================== delete category
export const REQUEST_DELETE_CATEGORY = 'REQUEST_DELETE_CATEGORY';
export const RECEIVE_DELETE_CATEGORY = 'RECEIVE_DELETE_CATEGORY';
export const ERROR_DELETE_CATEGORY = 'ERROR_DELETE_CATEGORY';

//=============Division Categories

//=============Get 
export const REQUEST_DIVISION_CATEGORIES = 'REQUEST_DIVISION_CATEGORIES';
export const RECEIVE_DIVISION_CATEGORIES = 'RECEIVE_DIVISION_CATEGORIES';
export const ERROR_DIVISION_CATEGORIES = 'ERROR_DIVISION_CATEGORIES';
//========= Save 
export const REQUEST_SAVE_DIVISION_CATEGORIES = 'REQUEST_SAVE_DIVISION_CATEGORIES';
export const RECEIVE_SAVE_DIVISION_CATEGORIES = 'RECEIVE_SAVE_DIVISION_CATEGORIES';
export const ERROR_SAVE_DIVISION_CATEGORIES = 'ERROR_SAVE_DIVISION_CATEGORIES';
//=========== Set division category (client side)
export const SET_DIVISION_AGES = 'SET_DIVISION_AGES';
export const SET_DIVISION_CATEGORIES = 'SET_DIVISION_GENDERS';
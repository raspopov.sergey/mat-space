﻿import * as at from './actionTypes';

const initialState = {
    isGettingAgeCategories: false,
    isGettingExperienceCategories: false,
    isGettingGenderCategories: false,
    isGettingWeightCategories: false,
    
    getAgeCategoriesError: '',
    getExperienceCategoriesError: '',
    getGenderCategoriesError: '',
    getWeightCategoriesError: '',

    categoryGroups: [],
    divisionCategories: [],
    saved: false
};

const reducer = (state = initialState, action) => {
    let groups;
    let group;
    let gidx;
    let cidx;
    let categories;
    switch (action.type) {

        // Groups
        case at.REQUEST_CATEGORIES:
            return { ...state, categoryGroups: [], isGettingAgeCategories: true };
        case at.RECEIVE_CATEGORIES:
            return { ...state, categoryGroups: action.groups };
        case at.RECEIVE_CATEGORIES_ERROR:
            return { ...state, isGettingAgeCategories: false, getAgeCategoriesError: action.error };

        // Add Group
        case at.REQUEST_ADD_CATEGORY_GROUP:
            return { ...state };
        case at.RECEIVE_ADD_CATEGORY_GROUP:
            return { ...state, categoryGroups: state.categoryGroups.concat(action.group) };
        case at.ERROR_ADD_CATEGORY_GROUP:
            return { ...state };

        // Update group
        case at.REQUEST_UPDATE_CATEGORY_GROUP:
            return { ...state };
        case at.RECEIVE_UPDATE_CATEGORY_GROUP:
            gidx = state.categoryGroups.findIndex(g => g.id === action.group.id);
            groups = [...state.categoryGroups];
            group = { ...groups[gidx] };
            group.name = action.group.name;
            groups[gidx] = group;
            return { ...state, categoryGroups: groups };
        case at.ERROR_UPDATE_CATEGORY_GROUP:
            return { ...state };

        // Delete  group
        case at.REQUEST_DELETE_CATEGORY_GROUP:
            return state;
        case at.RECEIVE_DELETE_CATEGORY_GROUP:
            return { ...state, categoryGroups: state.categoryGroups.filter(g => g.id !== action.group.id) };
        case at.ERROR_DELETE_CATEGORY_GROUP:
            return state;

        // Category
        // Add category 
        case at.REQUEST_ADD_CATEGORY:
            return { ...state };
        case at.RECEIVE_ADD_CATEGORY:
            gidx = state.categoryGroups.findIndex(g => g.id === action.category.categoryGroupId);
            groups = [...state.categoryGroups];
            group = { ...groups[gidx] };
            group.categories = group.categories ? group.categories.concat(action.category) : [action.category];
            groups[gidx] = group;
            return { ...state, categoryGroups: groups };
        case at.ERROR_ADD_CATEGORY:
            return { ...state, error: action.error };

        // Update category
        case at.REQUEST_UPDATE_CATEGORY:
            return {...state};
        case at.RECEIVE_UPDATE_CATEGORY:
            gidx = state.categoryGroups.findIndex(g => g.id === action.category.categoryGroupId);
            groups = [...state.categoryGroups];
            group = { ...groups[gidx] };
            cidx = group.categories.findIndex(c => c.id === action.category.id);
            categories = [...group.categories];
            categories[cidx] = action.category;
            group.categories = categories;
            groups[gidx] = group;
            return {...state, categoryGroups: groups};
        case at.ERROR_UPDATE_CATEGORY:
            return { ...state };

        // Delete category
        case at.REQUEST_DELETE_CATEGORY:
            return { ...state };
        case at.RECEIVE_DELETE_CATEGORY:
            gidx = state.categoryGroups.findIndex(g => g.id === action.category.categoryGroupId);
            groups = [...state.categoryGroups];
            group = { ...groups[gidx] };
            group.categories = group.categories.filter(c => c.id !== action.category.id);
            groups[gidx] = group;
            return { ...state, categoryGroups: groups };
        case at.ERROR_DELETE_CATEGORY:
            return { ...state };

        //Division Categories
        case at.REQUEST_DIVISION_CATEGORIES:
            return { ...state };
        case at.RECEIVE_DIVISION_CATEGORIES:
            return { ...state, divisionCategories: action.divisionCategories, categoryGroups: action.categories };
        case at.ERROR_DIVISION_CATEGORIES:
            return { ...state };
        
        case at.REQUEST_SAVE_DIVISION_CATEGORIES:
            return { ...state, saved: false };
        case at.RECEIVE_SAVE_DIVISION_CATEGORIES:
            return { ...state, divisionCategories: action.divisionCategories, saved: true };
        case at.ERROR_SAVE_DIVISION_CATEGORIES:
            return { ...state, saved: false };

        case at.SET_DIVISION_CATEGORIES:
            return { ...state, divisionCategories: action.categoryIds.map(id => { return { categoryId: id, divisionId: action.divisionId }; }) };
            

        default:
            return state;
    }
};

export default reducer;
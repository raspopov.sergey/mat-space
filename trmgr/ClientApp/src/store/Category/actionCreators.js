﻿import * as at from './actionTypes';

const headers = { 'Content-Type': 'application/json' };
const urlGetCategories = `api/organizer/categories`;

const actionCreators = {
    getCategoryGroups: (categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_CATEGORIES });
            const response = await fetch(`${urlGetCategories}?categoryType=${categoryType}`);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_CATEGORIES, groups: payload, categoryType: categoryType });
        }
        catch (err) {
            dispatch({ type: at.RECEIVE_CATEGORIES_ERROR, error: err });
        }
    },

    addCategoryGroup: (groupName, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_CATEGORY_GROUP });
            const group = { name: groupName };
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/AddAgeCategoryGroup';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/AddExperienceCategoryGroup';
                    break;
                case GENDER:
                    url = 'api/organizer/AddGenderCategoryGroup';
                    break;
                case WEIGHT:
                    url = 'api/organizer/AddWeightCategoryGroup';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: JSON.stringify(group), method: 'POST', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_ADD_CATEGORY_GROUP, group: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_ADD_CATEGORY_GROUP, error: err });
        }
    },

    updateCategoryGroup: (group, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_UPDATE_CATEGORY_GROUP });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/UpdateAgeCategoryGroup';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/UpdateExperienceCategoryGroup';
                    break;
                case GENDER:
                    url = 'api/organizer/UpdateGenderCategoryGroup';
                    break;
                case WEIGHT:
                    url = 'api/organizer/UpdateWeightCategoryGroup';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: JSON.stringify(group), method: 'PUT', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_UPDATE_CATEGORY_GROUP, group: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_UPDATE_CATEGORY_GROUP, error: err });
        }
    },

    deleteCategoryGroup: (groupId, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DELETE_CATEGORY_GROUP });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/DeleteAgeCategoryGroup';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/DeleteExperienceCategoryGroup';
                    break;
                case GENDER:
                    url = 'api/organizer/DeleteGenderCategoryGroup';
                    break;
                case WEIGHT:
                    url = 'api/organizer/DeleteWeightCategoryGroup';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: groupId, method: 'DELETE', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_DELETE_CATEGORY_GROUP, group: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DELETE_CATEGORY_GROUP });
        }
    },

    addCategory: (category, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_CATEGORY });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/AddAgeCategory';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/AddExperienceCategory';
                    break;
                case GENDER:
                    url = 'api/organizer/AddGenderCategory';
                    break;
                case WEIGHT:
                    url = 'api/organizer/AddWeightCategory';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: JSON.stringify(category), method: 'POST', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_ADD_CATEGORY, category: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_ADD_CATEGORY, error: err });
        }
    },
    
    updateCategory: (category, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_UPDATE_CATEGORY });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/UpdateAgeCategory';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/UpdateExperienceCategory';
                    break;
                case GENDER:
                    url = 'api/organizer/UpdateGenderCategory';
                    break;
                case WEIGHT:
                    url = 'api/organizer/UpdateWeightCategory';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: JSON.stringify(category), method: 'PUT', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_UPDATE_CATEGORY, category: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_UPDATE_CATEGORY });
        }
    },
    
    deleteCategory: (categoryId, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DELETE_CATEGORY });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/DeleteAgeCategory';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/DeleteExperienceCategory';
                    break;
                case GENDER:
                    url = 'api/organizer/DeleteGenderCategory';
                    break;
                case WEIGHT:
                    url = 'api/organizer/DeleteWeightCategory';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: categoryId, method: 'DELETE', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_DELETE_CATEGORY, category: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DELETE_CATEGORY});
        }
    },
    
    //======Division Categories
    getDivisionCategories: (divisionGroupId, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DIVISION_CATEGORIES });
            let urlCategories;
            let urlDivisionCategories;
            switch (categoryType) {
                case AGE:
                    urlCategories = `${urlGetCategories}?categoryType=${AGE}`;
                    urlDivisionCategories = `api/organizer/divisionages?divisionGroupId=${divisionGroupId}`;
                    break;
                case EXPERIENCE:
                    urlCategories = `${urlGetCategories}?categoryType=${EXPERIENCE}`;
                    urlDivisionCategories = `api/organizer/divisionexperiences?divisionGroupId=${divisionGroupId}`;
                    break;
                case GENDER:
                    urlCategories = `${urlGetCategories}?categoryType=${GENDER}`;
                    urlDivisionCategories = `api/organizer/divisiongenders?divisionGroupId=${divisionGroupId}`;
                    break;
                case WEIGHT:
                    urlCategories = `${urlGetCategories}?categoryType=${WEIGHT}`;
                    urlDivisionCategories = `api/organizer/divisionweights?divisionGroupId=${divisionGroupId}`;
                    break;
                default:
                    break;

            }
            const prCategories = fetch(urlCategories);
            const prDivisionCategories = fetch(urlDivisionCategories);
            const responses = await Promise.all([prCategories, prDivisionCategories]);
            const categories = await responses[0].json();
            const divisionCategories = await responses[1].json();
            dispatch({ type: at.RECEIVE_DIVISION_CATEGORIES, categoryType: categoryType, categories: categories, divisionCategories: divisionCategories });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DIVISION_CATEGORIES });
        }
    },
    saveDivisionCategories: (divisionGroup, categoryType) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_SAVE_DIVISION_CATEGORIES });
            let url;
            switch (categoryType) {
                case AGE:
                    url = 'api/organizer/divisionAges';
                    break;
                case EXPERIENCE:
                    url = 'api/organizer/divisionExperiences';
                    break;
                case GENDER:
                    url = 'api/organizer/divisionGenders';
                    break;
                case WEIGHT:
                    url = 'api/organizer/divisionWeights';
                    break;
                default:
                    break;
            }
            const response = await fetch(url, { body: JSON.stringify(divisionGroup), method: 'POST', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_SAVE_DIVISION_CATEGORIES, divisionCategories: payload, categoryType: categoryType });
        }
        catch (err) {
            dispatch({ type: at.ERROR_SAVE_DIVISION_CATEGORIES });
        }
    },
    setDivisionCategories: (divisionId, categoryIds, categoryType) => (dispatch) => {
        dispatch({ type: at.SET_DIVISION_CATEGORIES, divisionId: divisionId, categoryIds: categoryIds, categoryType: categoryType });
    }
    
};

export const AGE = 'AGE';
export const GENDER = 'GENDER';
export const EXPERIENCE = 'EXPERIENCE';
export const WEIGHT = 'WEIGHT';

export default actionCreators;
﻿import * as at from './actionTypes';

const initialState = {
    tournaments: [],
    divisionGroupId: null,
    tournament: null,
    divisionGroup: null,
    registration: null,
    registrationError: null,
    registrations: [],
    bracket: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case at.REQUEST_AVAILABLE_TOURNAMENTS:
            return { ...state };
        case at.RECEIVE_AVAILABLE_TOURNAMENTS:
            return { ...state, tournaments: action.tournaments };
        case at.ERROR_AVAILABLE_TOURNAMENTS:
            return { ...state };


        case at.REQUEST_TOURNAMENTS:
            return { ...state, divisionGroupId: null };
        case at.RECEIVE_TOURNAMENTS:
            return { ...state, tournaments: action.payload };
        case at.RECEIVE_TOURNAMENTS_ERROR:
            return { ...state };

        case at.REQUEST_TOURNAMENT:
            return { ...state, divisionGroupId: null };
        case at.RECEIVE_TOURNAMENT:
            return { ...state, tournament: action.tournament };
        case at.ERROR_TOURNAMENT:
            return { ...state };

        case at.REQUEST_ADD_TOURNAMENT:
            return { ...state };
        case at.RECEIVE_ADD_TOURNAMENT:
            return { ...state, tournaments: state.tournaments.concat(action.added) };
        case at.RECEIVE_ADD_TOURNAMENT_ERROR:
            return { ...state };

        case at.REQUEST_DELETE_TOURNAMENT:
            return { ...state };
        case at.RECEIVE_DELETE_TOURNAMENT:
            return { ...state, tournaments: state.tournaments.filter(t => t.id !== action.id) };
        case at.ERROR_DELETE_TOURNAMENT:
            return { ...state };


        case at.REQUEST_ADD_DIVISION:
            return { ...state };
        case at.RECEIVE_ADD_DIVISION:
            return {
                ...state, tournament: {
                    ...state.tournament, divisions: [
                        ...state.tournament.divisions, action.division]
                }
            };
        case at.ERROR_ADD_DIVISION:
            return { ...state };


        case at.REQUEST_ADD_DIVISION_GROUP:
            return { ...state };
        case at.RECEIVE_ADD_DIVISION_GROUP:
            return { ...state, divisionGroup: action.divisionGroup };
        case at.ERROR_ADD_DIVISION_GROUP:
            return { ...state };

        case at.REQUEST_DELETE_DIVISION_GROUP:
            return { ...state };
        case at.RECEIVE_DELETE_DIVISION_GROUP:
            const division = state.tournament.divisions.find(d => d.id === action.divisionGroup.divisionId);
            const groups = division.divisionGroups.filter(dg => dg.id !== action.divisionGroup.id);
            return {
                ...state, tournament: {
                    ...state.tournament, divisions: [
                        ...state.tournament.divisions.filter(d => d.id !== action.divisionGroup.divisionId), { ...division, divisionGroups: groups }]
                }
            };

        case at.ERROR_DELETE_DIVISION_GROUP:
            return { ...state };

        case at.REQUEST_BRACKETS:
            return { ...state, tournament: null };
        case at.RECEIVE_BRACKETS:
            return { ...state, tournament: action.tournament };
        case at.ERROR_BRACKETS:
            return { ...state };

        case at.REQUEST_BRACKET:
            return { ...state, tournament: null };
        case at.RECEIVE_BRACKET:
            return { ...state, bracket: action.bracket };
        case at.ERROR_BRACKET:
            return { ...state };

        
        case at.REQUEST_ADD_REGISTRATION:
            return { ...state, registration: null };
        case at.RECEIVE_ADD_REGISTRATION:
            return { ...state, registration: action.registration };
        case at.ERROR_ADD_REGISTRATION:
            return { ...state, registrationError: action.error };

        case at.REQUEST_REGISTRATIONS:
            return { ...state };
        case at.RECEIVE_REGISTRATIONS:
            return { ...state, registrations: action.registrations };
        case at.ERROR_REGISTRATIONS:
            return { ...state };
        
        default:
            return state;
        
    }
};

export default reducer;
﻿import * as at from './actionTypes';
import { post, get } from '../../utils/http';

const headers = { 'Content-Type': 'application/json' };

const actionCreators = {
    getAvailableTournaments: () => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_AVAILABLE_TOURNAMENTS });
            const url = `api/registration/available-tournaments`;
            const response = await fetch(url);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_AVAILABLE_TOURNAMENTS, tournaments: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_AVAILABLE_TOURNAMENTS });
        }
    },
    getTournaments: () => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_TOURNAMENTS });
            const url = `api/organizer/tournaments`;
            const response = await fetch(url);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_TOURNAMENTS, payload: payload });
        }
        catch (err) {
            dispatch({ type: at.RECEIVE_TOURNAMENTS_ERROR });
        }
    },
    getTournament: (tournamentId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_TOURNAMENT });
            const url = `api/registration/tournament?tournamentId=${tournamentId}`;
            const response = await fetch(url);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_TOURNAMENT, tournament: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_TOURNAMENT });
        }
    },
    addTournament: (tournament) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_TOURNAMENT });
            const url = `api/organizer/tournament`;
            const response = await fetch(url, { body: JSON.stringify(tournament), method: 'POST', headers: headers });
            console.log(response);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_ADD_TOURNAMENT, added: payload });
        }
        catch (err) {
            dispatch({ type: at.RECEIVE_ADD_TOURNAMENT_ERROR });
        }
    },
    deleteTournament: (id) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DELETE_TOURNAMENT });
            const url = 'api/organizer/tournament';
            const response = await fetch(url, { body: JSON.stringify(id), method: 'DELETE', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_DELETE_TOURNAMENT, id: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DELETE_TOURNAMENT });
        }
    },

    addDivision: (division) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_DIVISION });
            const url = 'api/organizer/division';
            const response = await fetch(url, { body: JSON.stringify(division), method: 'POST', headers: headers });
            const payload = await response.json();
            console.log(payload);
            dispatch({ type: at.RECEIVE_ADD_DIVISION, division: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_ADD_DIVISION });
        }
    },
    deleteDivision: (divisionId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DELETE_DIVISION });
            const url = 'api/organizer/divisiongroup';
            const response = await fetch(url, { body: divisionId, method: 'DELETE', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_DELETE_DIVISION, division: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DELETE_DIVISION });
        }
    },

    addDivisionGroup: (divisionId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_DIVISION_GROUP });
            const url = 'api/organizer/divisiongroup';
            const body = { divisionId: divisionId };
            const response = await fetch(url, { body: JSON.stringify(body), method: 'POST', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_ADD_DIVISION_GROUP, divisionGroup: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_ADD_DIVISION_GROUP });
        }
    },
    deleteDivisionGroup: (divisionGroupId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_DELETE_DIVISION_GROUP });
            const url = 'api/organizer/division-group';
            const response = await fetch(url, { body: divisionGroupId, method: 'DELETE', headers: headers });
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_DELETE_DIVISION_GROUP, divisionGroup: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_DELETE_DIVISION_GROUP });
        }
    },
    getBrackets: (tournamentId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_BRACKETS });
            const url = `api/organizer/brackets?tournamentId=${tournamentId}`;
            const response = await fetch(url);
            const payload = await response.json();
            dispatch({ type: at.RECEIVE_BRACKETS, tournament: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_BRACKETS });
        }
    },

    addRegistration: (registration) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_ADD_REGISTRATION });
            const payload = await post('api/registration/registration', registration);
            dispatch({ type: at.RECEIVE_ADD_REGISTRATION, registration: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_ADD_REGISTRATION, error: err });
        }
    },
    getRegistrations: (divisionGroupId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_REGISTRATIONS });
            const payload = await get(`api/registration/division-group-registrations?divisionGroupId=${divisionGroupId}`);
            dispatch({ type: at.RECEIVE_REGISTRATIONS, registrations: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_REGISTRATIONS });
        }
    },
    getBracket: (bracketId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_BRACKET });
            const payload = await get(`api/registration/bracket?bracketId=${bracketId}`);
            dispatch({ type: at.RECEIVE_BRACKET, bracket: payload });
        }
        catch (err) {
            dispatch({ type: at.ERROR_BRACKET });
        }
    }
};

export default actionCreators;
﻿import * as at from './actionTypes';

const actionCreators = {
    getCountries: () => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_COUNTRIES });
            const url = `api/address/countries`;
            const response = await fetch(url);
            const result = await response.json();
            dispatch({ type: at.RECEIVE_COUNTRIES, countries: result });
        }
        catch (err) {
            dispatch({ type: at.RECEIVE_COUNTRIES_ERROR, error: err });
        }
    },

    getProvinces: (countryId) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_PROVINCES });
            const url = `api/address/provinces?countryId=${countryId}`;
            const response = await fetch(url);
            const result = await response.json();
            dispatch({ type: at.RECEIVE_PROVINCES, provinces: result });
        }
        catch (err) {
            dispatch({ type: at.RECEIVE_PROVINCES_ERROR, error: err });
        }
    },

    getCities: (provinceId, city, limit) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_CITIES });
            const url = `api/address/cities?provinceId=${provinceId}&city=${city}&limit=${limit}`;
            const response = await fetch(url);
            const result = await response.json();
            dispatch({ type: at.RECEIVE_CITIES, cities: result });
        }
        catch (err) {
            dispatch({ type: at.RECIEVE_CITIES_ERROR, error: err });
        }
    },
    getClubs: (cityId, club, limit) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_CLUBS });
            const url = `api/address/clubs?cityId=${cityId}&club=${club}&limit=${limit}`;
            const response = await fetch(url);
            const result = await response.json();
            dispatch({ type: at.RECEIVE_CLUBS, clubs: result });
        }
        catch (err) {
            dispatch({ type: at.ERROR_CLUBS, error: err });
        }
    },
    getClubNames: (cityId, club, limit) => async (dispatch) => {
        try {
            dispatch({ type: at.REQUEST_CLUB_NAMES });
            const url = `api/address/club-names?cityId=${cityId}&club=${club}&limit=${limit}`;
            const response = await fetch(url);
            const result = await response.json();
            dispatch({ type: at.RECEIVE_CLUB_NAMES, clubNames: result });
        }
        catch (err) {
            dispatch({ type: at.ERROR_CLUB_NAMES, error: err });
        }
    }
};

export default actionCreators;
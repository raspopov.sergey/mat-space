﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
//import { Row, Col, Badge, Statistic, Tag } from 'antd';
import './Bracket.css';

class Bracket extends React.Component {
    componentDidMount() {
        this.props.getBracket(this.props.match.params['id']);
    }


    render() {

        let ctlBracket = [];
        if (this.props.bracket) {
            let roundMatches = [];
            let matches = this.props.bracket.matches;
            let ctlMatches = [];
            let round = 1;
            roundMatches = matches.filter(m => m.round === round);

            while (roundMatches.length > 0) {
                const baseHeight = 70;
                const baseMargin = 5;
                const edgeMargin = Math.pow(2, round - 2) * baseHeight + Math.pow(2, round - 1) * baseMargin - baseHeight / 2;//
                const midMargin = edgeMargin * 2;
                roundMatches.sort((a, b) => a.seedNumber > b.seedNumber);
                ctlMatches = roundMatches.map((m, idx, arr) => {
                    const length = arr.length;
                    let stl = { "margin-top": `${midMargin}px`, "margin-bottom": `${midMargin}px` };
                    if (idx === 0 === length - 1)
                        stl = { "margin-top": `${edgeMargin}px`, "margin-bottom": `${edgeMargin}px` };
                    else if (idx === 0)
                        stl = { "margin-top": `${edgeMargin}px`, "margin-bottom": `${midMargin}px` };
                    else if (idx === length - 1)
                        stl = { "margin-top": `${midMargin}px`, "margin-bottom": `${edgeMargin}px` };
                    stl = { ...stl, ...{ "height": `${baseHeight}px` } };

                    const r1 = m.registration1 ? <div>{m.registration1.firstName} {m.registration1.lastName}</div> : <div>-</div>;
                    const r2 = m.registration2 ? <div>{m.registration2.firstName} {m.registration2.lastName}</div> : <div>-</div>;
                    return <div key={m.id} className="bracket-match" style={stl}>{r1}{r2}</div>;
                });
                ctlMatches = <div className="round">{ctlMatches}</div>;
                ctlBracket.push(ctlMatches);
                round++;
                roundMatches = matches.filter(m => m.round === round);
            }
        }
        
        return (
            <div className="page-content">
                <h2>Bracket</h2>
                <div className="bracket">
                    {ctlBracket}
                </div>
            </div>
        );
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
)(Bracket);
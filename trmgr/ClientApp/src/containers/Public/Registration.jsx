﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
import addressActionCreators from '../../store/Address/actionCreators';
//import { Link } from 'react-router-dom';
//import { Button, Checkbox, Select, Option, Form, Progress, Alert, Steps } from 'antd';
//import EditForm from '../../components/Categories/EditForm';
//import AgeCategories from '../Organizer/Categories/AgeCategories';
import PersonalInfo from '../../components/Registration/PersonalInfo';
import CategoryChoice from '../../components/Registration/CategoryChoice';
import Complete from '../../components/Registration/Complete';
import './Registration.css';

class Registration extends React.Component {
    state = {
        registration: {},
        step: 1
    };
    
    componentDidMount() {
        this.props.getTournament(this.props.match.params['id']);
        this.props.getCountries();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.step === 2 && this.state.step === 3)
            this.props.addRegistration(this.state.registration);
    }

    handleSaveStep = (registration) => {
        this.setState({
            registration: { ...this.state.registration, ...registration },
            step: this.state.step + 1
        });
    }
    
    handleSubmitRegistration = (values) => {
    }

    render() {
        let content;
        switch (this.state.step) {
            case 1:
                content = <PersonalInfo onSubmit={this.handleSaveStep} tournament={this.props.tournament} />;
                break;
            case 2:
                content = <CategoryChoice onSubmit={this.handleSaveStep} tournament={this.props.tournament} />;
                break;
            case 3:
                content = <Complete registration={this.props.registration} error={this.props.registrationError} />;
                break;
            default:
                content = null;
                break;
        }

        if (this.props.tournament) {
            return (
                <div className="page-content">
                    <div className="registration-form">
                        {content}
                    </div>
                </div>
            );
        }
        else
            return null;
    }
}

export default connect(
    state => {
        return { ...state.tournament, ...state.address };
    },
    dispatch => bindActionCreators({ ...tournamentActionCreators, ...addressActionCreators}, dispatch)
)(Registration);
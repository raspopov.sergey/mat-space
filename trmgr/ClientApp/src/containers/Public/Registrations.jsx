﻿import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
import { Row, Col } from 'antd';
import './Registrations.css';

class Registrations extends React.Component {
    componentDidMount() {
        this.props.getRegistrations(this.props.match.params['id']);
    }

    handleRegistrationsClick = (registraion) => {

    }

    render() {
        const regs = this.props.registrations.map((r, i) => (<div key={i} className="list-item">
            <div className="list-item-content pointable" onClick={() => this.handleRegistrationsClick(r)}>
                <div className="registration">
                    <div className="registration-category">
                        <Row>
                            <Col xs={12} sm={6}>
                                <h5>Gender</h5>
                                <div>{r.genderCategory.name}</div>
                            </Col>
                            <Col xs={12} sm={6}>
                                <h5>Age</h5>
                                <div>{r.ageCategory.name}</div>
                            </Col>
                            <Col xs={12} sm={6}>
                                <h5>Experience</h5>
                                <div>{r.experienceCategory.name}</div>
                            </Col>
                            <Col xs={12} sm={6}>
                                <h5>Weight</h5>
                                <div>{r.weightCategory.name}</div>
                            </Col>
                        </Row>
                    </div>
                    <div className="registraion-count">
                        <Row>
                            <Col xs={24} sm={12}>
                                <h5>Registrations</h5>
                                <div>{r.registrationCount}</div>
                            </Col>
                            <Col xs={24} sm={12}>
                                <Link to={`/brackets/${r.bracketId}`}>Bracket</Link>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        </div>));
        return (
            <div className="page-content">
                <h2>Registrations</h2>
                <div className="list">{regs}</div>
            </div>
        );
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
    //state => { return { ...state.tournament, ...state.category } },
    //dispatch => bindActionCreators({...tournamentActionCreators, ...categoryActionCreators }, dispatch)
)(Registrations);
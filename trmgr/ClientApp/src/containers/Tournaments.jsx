﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../store/Tournament/actionCreators';
import { Link } from 'react-router-dom';
import { Button, Icon } from 'antd';
import './Tournaments.css';

class Tournaments extends React.Component {

    componentDidMount() {
        this.props.getAvailableTournaments();
    }

    render() {
        let user = JSON.parse(localStorage.getItem('user'));
        const signOptions = user !== null ? null : (<div className="sign-options">
            <Link to="/signin">
                <Button type="primary" className="btn-sign"><Icon type="login" />Sign In</Button>
            </Link>
            <Link to="/signup">
                <Button type="primary" className="btn-sign"><Icon type="user-add" />Create Account</Button>
            </Link>
        </div>);

        const tournaments = this.props.tournaments.map(t => (
            <div className="card" key={t.id}>
                <h4>{t.name}</h4>
                <div>
                    <div>
                        <h5>Date:</h5>
                        {t.date}
                    </div>
                    <div>
                        <h5>Registration Ends:</h5>
                        {t.registrationEnd}
                    </div>
                    <Link to={`/register/${t.id}`} className="ant-btn">Register</Link>
                </div>
            </div>));
        return (
            <div className="page-content">
                {signOptions}
                <h3>Tournaments</h3>
                <div className="cards">
                    {tournaments}
                </div>
            </div>
        );
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
)(Tournaments);
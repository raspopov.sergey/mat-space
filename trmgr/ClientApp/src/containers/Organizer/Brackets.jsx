﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
import categoryActionCreators from '../../store/Category/actionCreators';
import moment from 'moment';
import { Modal, Button, Tabs, Icon } from 'antd';
import { Link } from 'react-router-dom';

import EditForm from '../../components/Categories/EditForm';
import SelectedAges from '../../components/Categories/Selection/SelectedAges';
import SelectedWeights from '../../components/Categories/Selection/SelectedWeights';
import SimpleSelection from '../../components/Categories/Selection/SimpleSelection';
import Card from '../../components/UI/Card';
import CardActions from '../../components/UI/CardActions';

class Brackets extends React.Component {
    state = {
        selectedDivisionId: null
    }

    componentDidMount() {
        this.props.getBrackets(this.props.match.params['id']);
    }

    handleTabChange = (key) => {
        this.setState({ selectedDivisionId: key });
    }


    render() {
        
        if (this.props.tournament) {
            const ds = this.props.tournament.divisions.sort((d1, d2) => d1.id - d2.id).map(d => {
                const rs = d.registrations.map(r => {
                    var reg = r.registration;
                    return (<div>{`${reg.firstName} ${reg.lastName}`}</div>);
                });
                return (
                    <Tabs.TabPane key={d.id} tab={d.name} >
                        <div>{rs}</div>
                    </Tabs.TabPane>);
            });

            return (
                <div className="page-content">
                    <h2>{this.props.tournament.name}</h2>
                    <Tabs onChange={this.handleTabChange}>{ds}</Tabs>
                </div>);
        }
        else {
            return null;//mb redirect??
        }
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
)(Brackets);
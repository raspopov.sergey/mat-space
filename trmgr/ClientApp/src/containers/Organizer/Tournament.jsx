﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
//import categoryActionCreators from '../../store/Category/actionCreators';
//import moment from 'moment';
import { Modal, Button, Tabs } from 'antd';
//import { Link } from 'react-router-dom';

import EditForm from '../../components/Categories/EditForm';
import SelectedAges from '../../components/Categories/Selection/SelectedAges';
import SelectedWeights from '../../components/Categories/Selection/SelectedWeights';
import SimpleSelection from '../../components/Categories/Selection/SimpleSelection';
import Card from '../../components/UI/Card';
import CardActions from '../../components/UI/CardActions';

class Tournament extends React.Component {
    state = {
        inputs: {
            tournament: {
                name: {
                    placeholder: 'Name',
                    type: 'text'
                },
                date: {
                    placeholder: 'Tournament Date',
                    type: 'date'
                },
                regStartDate: {
                    placeholder: 'Registration Start',
                    type: 'date',
                    rules: [{ type: 'object' }]
                },
                regEndDate: {
                    placeholder: 'Registration End',
                    type: 'date',
                    rules: [{ type: 'object' }]
                }
            },
            division: {
                name: {
                    placeholder: 'Name',
                    type: 'text'
                }
            }
        },
        editedTournament: null,
        editedDivision: null,
        showFabOptions: false,
        selectedDivisionId: null
    }

    componentDidMount() {
        this.props.getTournament(this.props.match.params['id']);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.divisionGroup !== this.props.divisionGroup)
            this.props.history.push(`/division-groups/${this.props.divisionGroup.id}/ages`);
        else if (prevProps.tournament !== this.props.tournament)
            this.setState({ editedDivision: null });
    }

    handleTabChange = (key) => {
        this.setState({ selectedDivisionId: key });
    }

    handleAddGroupClick = () => {
        let selectedDivisionId = null;
        if (this.state.selectedDivisionId)
            selectedDivisionId = this.state.selectedDivisionId;
        else if (this.props.tournament.divisions && this.props.tournament.divisions.length > 0)
            selectedDivisionId = this.props.tournament.divisions[0].id;

        if (selectedDivisionId)    
            this.props.addDivisionGroup(selectedDivisionId);
        this.setState({ showFabOptions: false });
    }

    handleDeleteDivisionGroupClick = (divisionGroup) => {
        Modal.confirm({
            title: 'Delete Division Category',
            content: 'Are you sure you want to remove this group?',
            onOk: () => this.props.deleteDivisionGroup(divisionGroup.id),
            okText: 'Delete'
        });
    }

    handleAddDivisionClick = () => {
        this.setState({ showFabOptions: false, editedDivision: { tournamentId: this.props.match.params['id'] } });
    }

    handleAddDivision = (values) => {
        this.props.addDivision({ ...this.state.editedDivision, name: values.name });
    }

    handleCloseDivisionModal = () => {
        this.setState({ editedDivision: null });
    }

    handleRegistrationsClick = (dg) => {
        this.props.history.push(`/division-group/${dg.id}`);
    }

    render() {
        
        if (this.props.tournament) {
            const ds = this.props.tournament.divisions.sort((d1, d2) => d1.id - d2.id).map(d => {
                const dgs = d.divisionGroups.map(dg => {
                    const actions = [
                        { icon: 'edit', onClick: () => this.props.history.push(`/division-groups/${dg.id}/ages`) },
                        { icon: 'team', onClick: () => this.handleRegistrationsClick(dg) },
                        { icon: 'delete', onClick: () => this.handleDeleteDivisionGroupClick(dg) }
                    ];
                    const actionsControl = <CardActions actions={actions} />;
                    return (
                        <Card key={dg.id} footer={actionsControl}>
                            <SimpleSelection title="Genders" items={dg.genderCategories} />
                            <SimpleSelection title="Experience Levels" items={dg.experienceCategories} />
                            <div className="horizontal space-around">
                                <SelectedAges items={dg.ageCategories} />
                                <SelectedWeights items={dg.weightCategories} />
                            </div>
                        </Card>);
                });
                return (
                    <Tabs.TabPane key={d.id} tab={d.name} >
                        <div className="cards">{dgs}</div>
                    </Tabs.TabPane>);
            });

            const clsfab1 = this.state.showFabOptions ? "button-fab-option-1" : "button-fab-option-hidden";
            const clsfab2 = this.state.showFabOptions ? "button-fab-option-2" : "button-fab-option-hidden";
            return (
                <div className="page-content">
                    <h2>{this.props.tournament.name}</h2>
                    <Tabs onChange={this.handleTabChange}>{ds}</Tabs>

                    <Modal visible={this.state.editedDivision !== null} onCancel={this.handleCloseDivisionModal} footer={null} title="New Division" destroyOnClose>
                        <EditForm onSubmit={this.handleAddDivision} onCancel={this.handleCloseDivisionModal}
                            values={this.state.editedDivision} inputs={this.state.inputs.division} />
                    </Modal>

                    <Button onClick={() => this.setState({ showFabOptions: !this.state.showFabOptions })} icon="plus" shape="circle" type="primary" className="button-fab" size="large" />
                    <Button onClick={this.handleAddDivisionClick} shape="round" className={clsfab1}>New Division</Button>
                    <Button onClick={this.handleAddGroupClick} shape="round" className={clsfab2}>New Group</Button>
                </div>);
        }
        else {
            return null;//mb redirect??
        }
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
    //state => { return { ...state.tournament, ...state.category } },
    //dispatch => bindActionCreators({...tournamentActionCreators, ...categoryActionCreators }, dispatch)
)(Tournament);
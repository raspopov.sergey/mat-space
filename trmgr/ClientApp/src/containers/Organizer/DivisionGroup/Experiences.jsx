﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { EXPERIENCE } from '../../../store/Category/actionCreators';
import { Button } from 'antd';
import DivisionCategoryTree from '../../../components/Categories/DivisionCategoryTree'; 

class DivisionGroupAges extends React.Component {

    componentDidMount() {
        this.props.getDivisionCategories(this.props.match.params.id, EXPERIENCE);
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.saved && this.props.saved)
            this.props.history.push(`/division-groups/${this.props.match.params.id}/weights`);
    }

    handleOnCheck = (checkedKeys, info) => {
        this.props.setDivisionCategories(this.props.match.params.id, checkedKeys.map(k => +k).filter(k => k), EXPERIENCE);
    }

    handleSaveSelection = () => {
        const division = { id: this.props.match.params.id, experienceCategories: this.props.divisionCategories };
        this.props.saveDivisionCategories(division, EXPERIENCE);
    }

    render() {
        return (
            <div>
                <h4>Select experience levels</h4>
                <DivisionCategoryTree onCheck={this.handleOnCheck} divisionCategories={this.props.divisionCategories}
                    categoryGroups={this.props.categoryGroups} />
                <Button type="primary" onClick={this.handleSaveSelection}>Save</Button>
            </div>);
    }
}
export default connect(
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(DivisionGroupAges);
﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { AGE } from '../../../store/Category/actionCreators';
import { Button } from 'antd';
import DivisionCategoryTree from '../../../components/Categories/DivisionCategoryTree'; 

class DivisionGroupAges extends React.Component {
    
    componentDidMount() {
        this.props.getDivisionCategories(this.props.match.params.id, AGE);
    }

    componentDidUpdate(prevProps) {
        if (!prevProps.saved && this.props.saved)
            this.props.history.push(`/division-groups/${this.props.match.params.id}/genders`);
    }

    handleOnCheck = (checkedKeys, info) => {
        this.props.setDivisionCategories(this.props.match.params.id, checkedKeys.map(k => +k).filter(k => k), AGE);
    }

    handleSaveSelection = () => {
        const division = { id: this.props.match.params.id, ageCategories: this.props.divisionCategories };
        this.props.saveDivisionCategories(division, AGE);
    }

    render() {
        return (
            <div>
                <h4>Select age categories</h4>
                <DivisionCategoryTree onCheck={this.handleOnCheck} divisionCategories={this.props.divisionCategories}
                    categoryGroups={this.props.categoryGroups} />
                <Button type="primary" onClick={this.handleSaveSelection}>Save</Button>
            </div>);
    }
}
export default connect(
    //state => { return { ...state.tournament, ...state.category } },
    //dispatch => bindActionCreators({...tournamentActionCreators, ...actionCreators }, dispatch)
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(DivisionGroupAges);
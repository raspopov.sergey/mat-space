﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import tournamentActionCreators from '../../store/Tournament/actionCreators';
//import categoryActionCreators from '../../store/Category/actionCreators';
import moment from 'moment';
import { Modal, Button } from 'antd';
//import { Link } from 'react-router-dom';

import EditForm from '../../components/Categories/EditForm';
import Card from '../../components/UI/Card';
import CardActions from '../../components/UI/CardActions';

class OrganizerDashboard extends React.Component {
    state = {
        inputs: {
            tournament: {
                name: {
                    placeholder: 'Name',
                    type: 'text'
                },
                date: {
                    placeholder: 'Tournament Date',
                    type: 'date'
                },
                regStartDate: {
                    placeholder: 'Registration Start',
                    type: 'date',
                    rules: [{ type: 'object' }]
                },
                regEndDate: {
                    placeholder: 'Registration End',
                    type: 'date',
                    rules: [{ type: 'object' }]
                }
            },
            dynamicTournamentFields: {
                addButtonName: 'Add Division',
                placeholder: 'Division Name',
                rules: [{ required: true, whitespace: true, message: 'Please enter division name' }]
            },
            division: {
                name: {
                    placeholder: 'Name',
                    type: 'text'
                }
            }
        },
        editedTournament: null,
        editedDivision: null
    }

    componentDidMount() {
        this.props.getTournaments();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.tournaments !== this.props.tournaments)
            this.setState({ editedTournament: null, editedDivision: null });
        else if (!prevProps.divisionGroupId && this.props.divisionGroupId)//divisionGroupId resets on getTournaments
            this.props.history.push(`/division-groups/${this.props.divisionGroupId}/ages`);
    }

    handleNewTournamentClick = () => {
        this.setState({ editedTournament: {} });
    }

    handleEditTournamentClick = (tournament) => {
        //this.setState({ editedTournament: tournament });
        this.props.history.push(`/tournament/${tournament.id}`);
    }

    handleDeleteTournamentClick = (tournament) => {
        Modal.confirm({
            title: 'Delete Tournament',
            content: `Are you sure you want to remove ${tournament.name} tournament`,
            onOk: () => this.props.deleteTournament(tournament.id),
            okText: 'Delete'
        });
    }

    handleSubmitTournament = (values) => {
        const tournament = {
            name: values.name,
            date: values.date,
            registrationStart: values.regStart,
            registrationEnd: values.regEnd,
            divisions: values.names ? values.names.map(n => { return { name: n }; }) : null
        };
        this.props.addTournament(tournament);
    }
    
    handleNewDivisionClick = (tournament) => {
        this.props.addDivisionGroup(tournament.id);
    }

    handleGetBrackets = (tournamentId) => {
        this.props.history.push(`/brackets/${tournamentId}`);
    }

    render() {
        const tournaments = this.props.tournaments ? this.props.tournaments.map(t => {
            const divisions = t.divisions ? t.divisions.map(d => {
                return (<div key={d.id}>{d.name}</div>);
            }) : null;
            const actions = [
                { icon: 'setting', onClick: () => this.handleEditTournamentClick(t) },
                { icon: 'delete', onClick: () => this.handleDeleteTournamentClick(t) }
            ];
            const actionControl = <CardActions actions={actions} />

            return (
                <Card key={t.id} name={t.name} footer={actionControl}>
                    <div>Start Date: {moment(t.date).format('ddd MMM Do YYYY')}</div>
                    <div>Registration Starts: {moment(t.registrationStart).format('ddd MMM Do YYYY')}</div>
                    <div>Registration Ends: {moment(t.registrationEnd).format('ddd MMM Do YYYY')}</div>
                    <div>{divisions}</div>
                </Card>);
        }) : null;

        return (
            <div>
                <div className="cards">
                    {tournaments}
                </div>

                <Modal visible={this.state.editedTournament !== null} onCancel={() => this.setState({ editedTournament: null })}
                    footer={null} title="Tournament" destroyOnClose>
                    <EditForm onSubmit={this.handleSubmitTournament} onCancel={() => this.setState({ editedTournament: null })}
                        inputs={this.state.inputs.tournament} values={this.state.editedTournament} dynamicFieldProps={this.state.inputs.dynamicTournamentFields}/>
                </Modal>
                

                <Button onClick={this.handleNewTournamentClick} className="button-fab" icon="plus" size="large" type="primary" shape="circle"  />
            </div>
        );
    }
}

export default connect(
    state => state.tournament,
    dispatch => bindActionCreators(tournamentActionCreators, dispatch)
    //state => { return { ...state.tournament, ...state.category } },
    //dispatch => bindActionCreators({...tournamentActionCreators, ...categoryActionCreators }, dispatch)
)(OrganizerDashboard);
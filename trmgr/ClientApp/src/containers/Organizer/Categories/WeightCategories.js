﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { WEIGHT } from '../../../store/Category/actionCreators';
import { Modal, Button } from 'antd';

import ListItem from '../../../components/UI/ListItem';
import Card from '../../../components/UI/Card';
import CardActions from '../../../components/UI/CardActions';
import EditForm from '../../../components/Categories/EditForm';

class WeightCategories extends React.Component {
    state = {
        editedGroup: null,
        editedCategory: null,
        inputs: {
            group: {
                name: {
                    placeholder: 'Group Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter group name' }]
                }
            },
            category: {
                name: {
                    placeholder: 'Category Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter category name'}]
                },
                minWeight: {
                    placeholder: 'Min Weight',
                    type: 'number',
                    rules: [{ required: true, message: 'Please enter min weight'}]
                },
                maxWeight: {
                    placeholder: 'Max Weight',
                    type: 'number',
                    rules: [{ required: true, message: 'Please enter max weight' }]
                }
            }
        }
    }

    componentDidMount() {
        this.props.getCategoryGroups(WEIGHT);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categoryGroups !== this.props.categoryGroups)
            this.setState({ editedCategory: null, editedGroup: null });
    }

    handleNewGroupClick = () => {
        this.setState({ editedGroup: {}});
    }

    handleEditGroupClick = (group) => {
        this.setState({ editedGroup: {...group}});
    }

    handleDeleteGroupClick = (group) => {
        Modal.confirm({
            title: 'Delete Configrmation',
            content: `Are you sure you want to delete ${group.name} group?`,
            onOk: () => this.props.deleteCategoryGroup(group.id, WEIGHT),
            okText: 'Delete'
        });
    }

    handleCloseGroupEdit = () => {
        this.setState({ editedGroup: null });
    }

    handleGroupFormSubmit = (values) => {
        if (this.state.editedGroup.id) {
            const group = { id: this.state.editedGroup.id, name: values.name };
            this.props.updateCategoryGroup(group, WEIGHT);
        }
        else {
            this.props.addCategoryGroup(values.name, WEIGHT);
        }
    }



    handleNewCategoryClick = (groupId) => {
        this.setState({ editedCategory: { categoryGroupId: groupId }});
    }

    handleEditCategoryClick = (category) => {
        this.setState({ editedCategory: {...category}});
    }

    handleDeleteCategoryClick = (category) => {
        Modal.confirm({
            title: 'Delete Confirmation',
            content: `Are you sure you want to delete ${category.name} category?`,
            onOk: () => this.props.deleteCategory(category.id, WEIGHT),
            okText: 'Delete'
        });
    }
    
    handleCloseCategoryEdit = () => {
        this.setState({ editedCategory: null });
    }
    
    handleCategoryFormSubmit = (values) => {
        if (this.state.editedCategory.id) {
            const category = {
                id: this.state.editedCategory.id,
                name: values.name,
                minWeight: values.minWeight,
                maxWeight: values.maxWeight,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.updateCategory(category, WEIGHT);
        }
        else {
            const category = {
                id: this.state.editedCategory.id,
                name: values.name,
                minWeight: values.minWeight,
                maxWeight: values.maxWeight,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.addCategory(category, WEIGHT);
        }
    }
    
    render() {
        const groups = this.props.categoryGroups.map(group => {
            const categories = group.categories && group.categories.length > 0 ? group.categories.map(cat => (
                <ListItem key={cat.id} primaryText={cat.name} secondaryText={`${cat.minWeight} to ${cat.maxWeight} pounds`}
                    onItemClick={() => this.handleEditCategoryClick(cat)} onDeleteClick={() => this.handleDeleteCategoryClick(cat)} />)) :
                <h3 className="secondary-text">No Weight Categories</h3>;
            const actions = [
                { icon: 'plus', onClick: () => this.handleNewCategoryClick(group.id) },
                { icon: 'edit', onClick: () => this.handleEditGroupClick(group) },
                { icon: 'delete', onClick: () => this.handleDeleteGroupClick(group) }
            ];
            const actionControl = <CardActions actions={actions} />;
            return (
                <Card key={group.id} name={group.name} footer={actionControl}>
                    {categories}
                </Card>);
        });
        return (
            <div>
                <div className="cards">
                    {groups}
                </div>

                <Button onClick={this.handleNewGroupClick} className="button-fab" type="primary" shape="circle" size="large" icon="plus" />

                <Modal visible={this.state.editedGroup !== null} onCancel={this.handleCloseGroupEdit} title="Weight Group" footer={null} destroyOnClose>
                    <EditForm inputs={this.state.inputs.group} values={this.state.editedGroup}
                        onSubmit={this.handleGroupFormSubmit} onCancel={this.handleCloseGroupEdit} />
                </Modal>

                <Modal visible={this.state.editedCategory !== null} onCancel={this.handleCloseCategoryEdit}
                    title="Weight Category" footer={null} destroyOnClose>
                    <EditForm inputs={this.state.inputs.category} values={this.state.editedCategory}
                        onSubmit={this.handleCategoryFormSubmit} onCancel={this.handleCloseCategoryEdit} />
                </Modal>
            </div>);
    }
}

export default connect(
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(WeightCategories);
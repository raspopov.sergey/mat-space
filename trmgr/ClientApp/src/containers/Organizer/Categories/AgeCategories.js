﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { AGE } from '../../../store/Category/actionCreators';

import { Modal, Button } from 'antd';
import CardActions from '../../../components/UI/CardActions';
import ListItem from '../../../components/UI/ListItem';
import Card from '../../../components/UI/Card';
import EditForm from '../../../components/Categories/EditForm';

class AgeCategories extends React.Component {
    state = {
        editedCategory: null,
        editedGroup: null,
        inputs: {
            group: {
                name: {
                    placeholder: 'Group Name',
                    rules: [{ required: true, message: 'Please enter age category name' }],
                    type: 'text'
                }
            },
            category: {
                name: {
                    placeholder: 'Category Name',
                    rules: [{ required: true, message: 'Please enter age category name' }],
                    type: 'text'
                },
                minAge: {
                    placeholder: 'Min Age',
                    rules: [{ required: true, message: 'Please enter min age' }],
                    type: 'number'
                },
                maxAge: {
                    placeholder: 'Max Age',
                    rules: [{ required: true, message: 'Please enter max age' }],
                    type: 'number'
                }
            }
        }
    }

    componentDidMount() {
        this.props.getCategoryGroups(AGE);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categoryGroups !== this.props.categoryGroups)
            this.setState({ editedCategory: null, editedGroup: null });
    }

    //category
    handleNewCategoryClick = (groupId) => {
        this.setState({ editedCategory: { categoryGroupId: groupId} });
    }

    handleEditCategoryClick = (category) => {
        this.setState({ editedCategory: { ...category } });
    }

    handleDeleteCategoryClick = (category) => {
        Modal.confirm({
            title: "Delete Category",
            content: `Are you sure you wate to delete ${category.name} age category?`,
            onOk: () => this.props.deleteCategory(category.id, AGE),
            okText: "Delete"
        });
    }
    
    handleCloseCategoryEdit = () => {
        this.setState({ editedCategory: null });
    }

    handleCategoryFormSubmit = (values) => {
        if (this.state.editedCategory.id) {
            const category = {
                id: this.state.editedCategory.id,
                categoryGroupId: this.state.editedCategory.categoryGroupId,
                name: values.name,
                minAge: values.minAge,
                maxAge: values.maxAge
            };
            this.props.updateCategory(category, AGE);
        }
        else {
            const category = {
                categoryGroupId: this.state.editedCategory.categoryGroupId,
                name: values.name,
                minAge: values.minAge,
                maxAge: values.maxAge
            };
            this.props.addCategory(category, AGE);
        }
    }

    //group
    handleNewGroupClick = () => {
        this.setState({ editedGroup: {} });
    }
    
    handleEditGroupClick = (group) => {
        this.setState({ editedGroup: {...group } });
    }
    
    handleDeleteGroupClick = (group) => {
        Modal.confirm({
            title: "Delete Group",
            content: `Are you sure you want to delete ${group.name} age category group?`,
            onOk: () => this.props.deleteCategoryGroup(group.id, AGE),
            okText: "Delete"
        });
    }
    
    handleCloseGroupEdit = () => {
        this.setState({ editedGroup: null });
    }
    
    handleGroupFormSubmit = (values) => {
        if (this.state.editedGroup.id > 0) {
            const group = { id: this.state.editedGroup.id, name: values.name };
            this.props.updateCategoryGroup(group, AGE);
        }
        else {
            this.props.addCategoryGroup(values.name, AGE);
        }
    }
    
    
    render() {
        const groups = this.props.categoryGroups.map(group => {
            const categories = group.categories && group.categories.length > 0 ? group.categories.map(cat => (
                <ListItem key={cat.id} primaryText={cat.name} secondaryText={`${cat.minAge} to ${cat.maxAge} years`}
                    onItemClick={() => this.handleEditCategoryClick(cat)} onDeleteClick={() => this.handleDeleteCategoryClick(cat)} />)) :
                <h3 className="secondary-text">No Age Categories</h3>;
            const actions = [
                { icon: 'plus', onClick: () => this.handleNewCategoryClick(group.id) },
                { icon: 'edit', onClick: () => this.handleEditGroupClick(group) },
                { icon: 'delete', onClick: () => this.handleDeleteGroupClick(group) }
            ];
            const actionControl = <CardActions actions={actions} />;
            return (
                <Card key={group.id} name={group.name} footer={actionControl}>
                    {categories}
                </Card>
            );
        });

        return (
            <div>
                <div className="cards">
                    {groups}
                </div>

                <Button onClick={this.handleNewGroupClick} icon="plus" shape="circle" type="primary" className="button-fab" size="large"/>

                <Modal footer={null} destroyOnClose title="Age Group" visible={this.state.editedGroup !== null} onCancel={this.handleCloseGroupEdit}>
                    <EditForm onSubmit={this.handleGroupFormSubmit} onCancel={this.handleCloseGroupEdit}
                        values={this.state.editedGroup} inputs={this.state.inputs.group} />
                </Modal>

                <Modal visible={this.state.editedCategory !== null} footer={null} destroyOnClose
                    onCancel={this.handleCloseCategoryEdit} title="Age Category" >
                    <EditForm onSubmit={this.handleCategoryFormSubmit} onCancel={this.handleCloseCategoryEdit}
                        values={this.state.editedCategory} inputs={this.state.inputs.category} />
                </Modal>

                

            </div>);
    }
}

export default connect(
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(AgeCategories);
﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { EXPERIENCE } from '../../../store/Category/actionCreators';
import { Modal, Button } from 'antd';

import ListItem from '../../../components/UI/ListItem';
import CardActions from '../../../components/UI/CardActions';
import Card from '../../../components/UI/Card';
import EditForm from '../../../components/Categories/EditForm';

class ExperienceCategories extends React.Component {
    state = {
        editedGroup: null,
        editedCategory: null,
        inputs: {
            group: {
                name: {
                    placeholder: 'Group Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter group name' }]
                }
            },
            category: {
                name: {
                    placeholder: 'Category Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter category name' }]
                },
                difficulty: {
                    placeholder: 'Difficulty',
                    type: 'number',
                    rules: [{ required: true, message: 'Please enter difficulty'}]
                }
            }
        }
    }

    componentDidMount() {
        this.props.getCategoryGroups(EXPERIENCE);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categoryGroups !== this.props.categoryGroups)
            this.setState({ editedGroup: null, editedCategory: null });
    }

    //group
    handleNewGroupClick = () => {
        this.setState({ editedGroup: {} });
    }

    handleEditGroupClick = (group) => {
        this.setState({ editedGroup: {...group} });
    }

    handleDeleteGroupClick = (group) => {
        Modal.confirm({
            title: "Delete Group",
            content: `Are you sure you wate to delete ${group.name} experience group?`,
            onOk: () => this.props.deleteCategoryGroup(group.id, EXPERIENCE),
            okText: "Delete"
        });
    }

    handleCloseGroupEdit = () => {
        this.setState({ editedGroup: null });
    }

    handleGroupFormSubmit = (values) => {
        if (this.state.editedGroup.id > 0) {
            const group = { id: this.state.editedGroup.id, name: values.name };
            this.props.updateCategoryGroup(group, EXPERIENCE);
        }
        else {
            this.props.addCategoryGroup(values.name, EXPERIENCE);
        }
    }


    handleNewCategoryClick = (groupId) => {
        const categories = this.props.categoryGroups.find(c => c.id === groupId).categories;
        const newDifficulty = categories ? Math.max.apply(null, categories.map(c => c.difficulty)) + 1 : 1;
        this.setState({ editedCategory: { categoryGroupId: groupId, difficulty: newDifficulty } });
    }
    
    handleEditCategoryClick = (category) => {
        this.setState({ editedCategory: {...category}});
    }
    
    handleDeleteCategoryClick = (category) => {
        Modal.confirm({
            title: 'Delete Category',
            content: `Are you sure you want to delete ${category.name} experience category?`,
            onOk: () => this.props.deleteCategory(category.id, EXPERIENCE),
            okText: 'Delete'
        });
    }
    
    handleCloseCategoryEdit = () => {
        this.setState({ editedCategory: null });
    }
    
    handleCategoryFormSubmit = (values) => {
        if (this.state.editedCategory.id) {
            const category = {
                id: this.state.editedCategory.id,
                name: values.name,
                difficulty: values.difficulty,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.updateCategory(category, EXPERIENCE);
        }
        else {
            const category = {
                name: values.name,
                difficulty: values.difficulty,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.addCategory(category, EXPERIENCE);
        }
    }


    render() {
        const groups = this.props.categoryGroups.map(group => {
            let categories = [];
            if (group.categories && group.categories.length > 0) {
                group.categories.sort((a, b) => a.difficulty > b.difficulty);
                categories = group.categories.map(cat => (
                    <ListItem key={cat.id} primaryText={cat.name}
                        onItemClick={() => this.handleEditCategoryClick(cat)} onDeleteClick={() => this.handleDeleteCategoryClick(cat)} />));
            }
            else {
                categories = [...<h3 className="secondary-text">No Experience Categories</h3>];
            }

            const actions = [
                { icon: 'plus', onClick: () => this.handleNewCategoryClick(group.id) },
                { icon: 'edit', onClick: () => this.handleEditGroupClick(group) },
                { icon: 'delete', onClick: () => this.handleDeleteGroupClick(group) }
            ];
            const actionControl = <CardActions actions={actions} />;
            return (
                <Card key={group.id} name={group.name} footer={actionControl}>
                    {categories}
                </Card>);
        });
        
        return (
            <div>
                <div className="cards">
                    {groups}
                </div>

                <Button onClick={this.handleNewGroupClick} icon="plus" shape="circle" type="primary" className="button-fab" size="large" />

                <Modal visible={this.state.editedGroup !== null} footer={null} onCancel={this.handleCloseGroupEdit} title="Experience Group" destroyOnClose>
                    <EditForm onSubmit={this.handleGroupFormSubmit} onCancel={this.handleCloseGroupEdit}
                        values={this.state.editedGroup} inputs={this.state.inputs.group} />
                </Modal>

                <Modal visible={this.state.editedCategory !== null} footer={null} onCancel={this.handleCloseCategoryEdit} title="Experience Category" destroyOnClose>
                    <EditForm onSubmit={this.handleCategoryFormSubmit} onCancel={this.handleCloseCategoryEdit}
                        values={this.state.editedCategory} inputs={this.state.inputs.category} />
                </Modal>
            </div>);
    }
}

export default connect(
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(ExperienceCategories);
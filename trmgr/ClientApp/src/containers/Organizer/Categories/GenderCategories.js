﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators, { GENDER } from '../../../store/Category/actionCreators';
import { Modal, Button } from 'antd';

import ListItem from '../../../components/UI/ListItem';
import Card from '../../../components/UI/Card';
import CardActions from '../../../components/UI/CardActions';
import EditForm from '../../../components/Categories/EditForm';

class GenderCategories extends React.Component {
    state = {
        editedGroup: null,
        editedCategory: null,
        inputs: {
            group: {
                name: {
                    placeholder: 'Group Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter group name' }]
                }
            },
            category: {
                name: {
                    placeholder: 'Category Name',
                    type: 'text',
                    rules: [{ required: true, message: 'Please enter category name' }]
                }
            }
        }
    }

    componentDidMount() {
        this.props.getCategoryGroups(GENDER);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categoryGroups !== this.props.categoryGroups)
            this.setState({ editedGroup: null, editedCategory: null });
    }

    //group
    handleNewGroupClick = () => {
        this.setState({ editedGroup: {} });
    }

    handleEditGroupClick = (group) => {
        this.setState({ editedGroup: {...group}});
    }

    handleDeleteGroupClick = (group) => {
        Modal.confirm({
            title: 'Delete Group',
            content: `Are you sure you want to delete ${group.name} gender group?`,
            onOk: () => this.props.deleteCategoryGroup(group.id, GENDER),
            okText: 'Delete'
        });
    }

    handleCloseGroupEdit = () => {
        this.setState({ editedGroup: null });
    }

    handleGroupFormSubmit = (values) => {
        if (this.state.editedGroup.id > 0) {
            const group = { id: this.state.editedGroup.id, name: values.name };
            this.props.updateCategoryGroup(group, GENDER);
        }
        else {
            this.props.addCategoryGroup(values.name, GENDER);
        }
    }

    //category
    handleNewCategoryClick = (groupId) => {
        this.setState({ editedCategory: { categoryGroupId: groupId }});
    }

    handleEditCategoryClick = (category) => {
        this.setState({ editedCategory: {...category}});
    }

    handleDeleteCategoryClick = (category) => {
        Modal.confirm({
            title: 'Delete Category',
            content: `Are you sure you want to delete ${category.name} category?`,
            onOk: () => this.props.deleteCategory(category.id, GENDER),
            okText: 'Delete'
        });
    }
    
    handleCloseCategoryEdit = () => {
        this.setState({ editedCategory: null });
    }
    
    handleCategoryFormSubmit = (values) => {
        if (this.state.editedCategory.id) {
            const category = {
                id: this.state.editedCategory.id,
                name: values.name,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.updateCategory(category, GENDER);
        }
        else {
            const category = {
                name: values.name,
                categoryGroupId: this.state.editedCategory.categoryGroupId
            };
            this.props.addCategory(category, GENDER);
        }
    }
    

    render() {
        const groups = this.props.categoryGroups.map(group => {
            const categories = group.categories && group.categories.length > 0 ? group.categories.map(cat => (
                <ListItem key={cat.id} primaryText={cat.name}
                    onItemClick={() => this.handleEditCategoryClick(cat)} onDeleteClick={() => this.handleDeleteCategoryClick(cat)} />)) :
                <h3 className="secondary-text">No Gender Categories</h3>;
            const actions = [
                { icon: 'plus', onClick: () => this.handleNewCategoryClick(group.id) },
                { icon: 'edit', onClick: () => this.handleEditGroupClick(group) },
                { icon: 'delete', onClick: () => this.handleDeleteGroupClick(group) }
            ];
            const actionControl = <CardActions actions={actions} />;
            return (
                <Card key={group.id} name={group.name} footer={actionControl}>
                    {categories}
                </Card>
            );
        });

        return (
            <div>
                <div className="cards">
                    {groups}
                </div>

                <Button onClick={this.handleNewGroupClick} icon="plus" type="primary" shape="circle" className="button-fab" size="large" />

                <Modal visible={this.state.editedGroup !== null} onCancel={this.handleCloseGroupEdit} footer={null} title="Gender Group" destroyOnClose>
                    <EditForm onSubmit={this.handleGroupFormSubmit} onCancel={this.handleCloseGroupEdit}
                        values={this.state.editedGroup} inputs={this.state.inputs.group} />
                </Modal>

                <Modal visible={this.state.editedCategory !== null} onCancel={this.handleCloseCategoryEdit} footer={null} title="Gender Category" destroyOnClose>
                    <EditForm onSubmit={this.handleCategoryFormSubmit} onCancel={this.handleCloseCategoryEdit}
                        values={this.state.editedCategory} inputs={this.state.inputs.category} />
                </Modal>
            </div>);
    }
}

export default connect(
    state => state.category,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(GenderCategories);
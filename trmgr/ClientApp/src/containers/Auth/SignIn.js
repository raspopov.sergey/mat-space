﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators from '../../store/Auth/actionCreators';

import { Form, Icon, Input, Button } from 'antd';

class Login extends React.Component {
    state = {
        inputs: {
            userName: {
                icon: { name: 'user', style: { color: 'rgba(0, 0, 0, .25)' } },
                placeholder: 'User Name',
                rules: [{ required: true, message: 'Please enter your user name' }],
                type: 'text'
            },
            password: {
                icon: { name: 'lock', style: { color: 'rgba(0, 0, 0, .25)' } },
                placeholder: 'Password',
                rules: [{ required: true, message: 'Please enter your password' }],
                type: 'password'
            }
        }
    }
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.signIn(values.userName, values.password);
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const inputs = Object.keys(this.state.inputs).map(i => {
            const input = this.state.inputs[i];
            const icon = input.icon ? <Icon type={input.icon.name} style={input.icon.style} /> : null;
            const field = getFieldDecorator(i, { rules: input.rules })(<Input prefix={icon} type={input.type} placeholder={input.placeholder} />);
            return <Form.Item key={i}>{field}</Form.Item>;
        });

        return (
            <Form onSubmit={this.handleSubmit} className="login-form">
                <h2>Login</h2>
                {inputs}
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Log in
                    </Button>
                </Form.Item>
                <div>
                    <Button className="login-form-button" onClick={() => this.props.history.push('/signup')}>Create an Account</Button>
                </div>
            </Form>
        );
    }
}

export default connect(
    state => state.auth,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(Form.create({ name: 'normal_login' })(Login));
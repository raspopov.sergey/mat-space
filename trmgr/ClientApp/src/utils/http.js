﻿const headers = { 'Content-Type': 'application/json' };

export const post = async (url, body) => {
    const response = await fetch(url, { body: JSON.stringify(body), method: 'POST', headers: headers });
    if (!response.ok)
        throw await response.text();
    return await response.json();
};

export const get = async (url) => {
    const response = await fetch(url);
    if (!response.ok)
        throw await response.text();
    return await response.json();
}
﻿import React from 'react';
import MainHeader from './MainHeader';

const wrapper = (props) => {
    return (
        <div>
            <MainHeader />
            {props.children}
        </div>
    );
};

export default wrapper;
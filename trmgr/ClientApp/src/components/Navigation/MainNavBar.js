﻿import React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu, Drawer } from 'antd';

const mainNavBar = (props) => {
    const navItems = props.items.map(i => (
        <Menu.Item key={i.text}>
            <NavLink to={i.path} onClick={props.onClose}>{i.text}</NavLink>
        </Menu.Item>));
    return (
        <Drawer visible={props.visible} onClose={props.onClose} placement="left" title="Mat Space" bodyStyle={{ padding: 0}}>
            <Menu mode="inline">
                {navItems}
            </Menu>
        </Drawer>
    );
};

export default mainNavBar;
﻿import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon } from 'antd';

const mainHeader = (props) => {
    const home = !props.isSignedIn ? (
        <Link to="/tournaments">
            <Button>
                <Icon type="home" />
                Home
            </Button>
        </Link>
    ) : null;

    const menu = props.isSignedIn ? (
        <Button onClick={props.onMenuOpen}>
            <Icon type="menu" />
        </Button>
    ) : null;

    const signOut = props.isSignedIn ? (
        <Button onClick={props.onSignOut}>
            <Icon type="logout" />
            Sign Out
        </Button>
    ) : null;
    
    return (
        <div className="main-header">
            <div>
                {home}
                {menu}
            </div>
            <div>
                {signOut}
            </div>
        </div>);
};

export default mainHeader;
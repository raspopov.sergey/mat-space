﻿import React from 'react';

const card = (props) => {
    const header = props.name ? (
        <div className="card-header">
            <h3>{props.name}</h3>
        </div>) : null;
        
    return (
        <div className="card">
            <div>
                {header}
                <div className="card-body">
                    {props.children}
                </div>
            </div>
            <div>
                {props.footer}
            </div>
        </div>);
};

export default card;
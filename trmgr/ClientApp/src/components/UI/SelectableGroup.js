﻿import React from 'react';
import { IconButton, ActionButton } from 'office-ui-fabric-react/lib/Button';

const listItemGroup = (props) => {
    return (
        <div key={ag.id}>
            <ActionButton onClick={() => this.handleSelectAgeGroup(ag)}>{ag.name}</ActionButton>

            {ag.ageCategories.map(ac => {
                const selected = this.state.selectedAgeCategoryIds.indexOf(ac.id) >= 0;
                const icon = selected ? "SkypeCircleCheck" : "StatusCircleRing";
                return (
                    <div>
                        <ActionButton key={ac.id} iconProps={{ iconName: icon }} onClick={() => this.handleSelecteAgeCategory(ac, !selected)}>
                            {ac.name} {ac.minAge} - {ac.maxAge}
                        </ActionButton>
                    </div>
                );
            })}
        </div>);
};

export default listItemGroup;
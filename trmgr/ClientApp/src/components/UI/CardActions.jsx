﻿import React from 'react';
import { Icon } from 'antd';

const actions = (props) => {
    const actions = props.actions.map((a, i) => (<div key={i} className="card-action" onClick={a.onClick}><Icon type={a.icon} />{a.text}</div>));
    //<div className="card-action-separator"></div>
    return (
        <div className="card-actions">
            {actions}
        </div>
    );
};

export default actions;
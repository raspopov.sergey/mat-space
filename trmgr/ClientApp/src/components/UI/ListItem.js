﻿import React from 'react';
import { Icon } from 'antd';

const listItem = (props) => (
    <div className="list-item" >
        <div className="list-item-content pointable" onClick={props.onItemClick}>
            {props.primaryText}
            <div className="secondary-text">{props.secondaryText}</div>
        </div>
        <div className="list-item-controls">
            <Icon onClick={props.onDeleteClick} type="delete" style={{ color: 'red' }} />
        </div>
    </div>
);

export default listItem;
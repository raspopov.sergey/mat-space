﻿import React from 'react';
import { Form, Input, Button, DatePicker, Icon, Select } from 'antd';

class RegistrationForm extends React.Component {
    state = {
        saving: false
    }
    
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({ loading: true });
                this.props.onSubmit(values);
            }
        });
    }

    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const inputs = Object.keys(this.props.inputs).map((i, idx) => {
            const input = this.props.inputs[i];
            let field;
            if (input.type === 'date') {
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values[i] })
                    (<DatePicker placeholder={input.placeholder} style={{ width: '100%' }} autoFocus={idx === 0} />);
            }
            else if (input.type === 'select') {
                const options = input.options.map(o => <Select.Option key={input[input.valueField]} value={input[input.valueField]}>input[input.textField]</Select.Option>);
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values[i] })(
                    <Select placeholder={input.placeholder} style={{ width: '100%' }} autoFocus={idx === 0}>
                        {options}
                    </Select>);
            }
            else {
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values[i] })(
                    <Input type={input.type} placeholder={input.placeholder} autoFocus={idx === 0} step="any" />);
            }
            return <Form.Item label={input.placeholder} key={i}>{field}</Form.Item>;
        });
        
        return (
            <Form onSubmit={this.handleSubmit}>
                {inputs}
                <div className="form-buttons">
                    <Button className="form-button" onClick={this.props.onCancel}>Cancel</Button>
                    <Button className="form-button" type="primary" htmlType="submit" loading={this.state.loading}>Save</Button>
                </div>
            </Form>);
    }
}

export default Form.create({ name: 'registration-form' })(RegistrationForm);
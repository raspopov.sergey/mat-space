﻿import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actionCreators from '../../store/Address/actionCreators';
import EditForm from '../Categories/EditForm';

class PersonalInfo extends React.Component {
    state = {
        inputs: {
            firstName: {
                placeholder: 'First Name',
                type: 'text',
                rules: [{ required: true, message: 'Please enter your first name' }]
            },
            lastName: {
                placeholder: 'Last Name',
                type: 'text',
                rules: [{ required: true, message: 'Please enter your last name' }]
            },
            emailAddress: {
                placeholder: 'Email Address',
                type: 'email',
                rules: [{ required: true, message: 'Please enter your email address' }]
            },
            city: {
                type: 'auto-complete',
                placeholder: 'City',
                options: this.props.cities,
                valueField: 'id',
                textField: 'name',
                rules: [],
                onSearch: (e) => this.handleCityNameChanged(e),
                onSelect: (e) => this.handleCityNameSelected(e)
            },
            club: {
                type: 'auto-complete',
                placeholder: 'Club',
                options: [],
                valueField: 'id',
                textField: 'name',
                rules: [],
                onSearch: (e) => this.handleClubNameChanged(e),
                onSelect: (e) => this.handleClubNameSelected(e)
            }
        }
    }

    componentDidMount() {
        
    }

    componentDidUpdate(prevProps) {
        if (prevProps.cities !== this.props.cities)
            this.setState({ inputs: { ...this.state.inputs, city: { ...this.state.inputs.city, options: this.props.cities } } });
        else if (prevProps.clubNames !== this.props.clubNames)
            this.setState({ inputs: { ...this.state.inputs, club: { ...this.state.inputs.club, options: this.props.clubNames } } });
    }

    handleSubmitPersonalInfo = (values) => {
        var registration = {
            tournamentId: this.props.tournament.id,
            firstName: values.firstName,
            lastName: values.lastName,
            emailAddress: values.emailAddress,
            city: values.city,
            club: values.club
        };
        this.props.onSubmit(registration);
    }

    handleCityNameChanged = (value) => {
        this.props.getCities(0, value, 5);
    }

    handleCityNameSelected = (value) => {
        console.log(value);

    }

    handleClubNameChanged = (value) => {
        this.props.getClubNames(0, value, 5);
    }

    handleClubNameSelected = (value) => {

    }

    render() {
        if (this.props.tournament) {
            return (
                <div>
                    <h4>Personal Information</h4>
                    <EditForm onSubmit={this.handleSubmitPersonalInfo} onCancel={null}
                        inputs={this.state.inputs} values={this.state.formValues} okText="Next" />
                </div>
            );
        }
        else
            return null;
    }
}

export default connect(
    state => state.address,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(PersonalInfo);

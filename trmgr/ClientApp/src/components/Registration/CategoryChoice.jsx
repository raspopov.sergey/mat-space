﻿import React from 'react';
import EditForm from '../Categories/EditForm';

class CategoryChoice extends React.Component {
    state = {
        inputs: {
            divisions: {
                type: 'checkbox-group',
                options: this.props.tournament.divisions,
                valueField: 'id',
                textField: 'name',
                rules: [{ required: true, message: 'Please select at least one division.' }],
                onChange: (e) => this.handleDivisionSelectedChanged(e)
            }
        }
    }
    
    handleDivisionSelectedChanged = (divisionIds) => {
        let inputs = { ...this.state.inputs };
        this.props.tournament.divisions.forEach(d => {
            const selected = divisionIds.some(id => id === d.id);
            const gInput = this.state.inputs[`gender-${d.id}`];
            const aInput = this.state.inputs[`age-${d.id}`];
            const eInput = this.state.inputs[`experience-${d.id}`];
            const wInput = this.state.inputs[`weight-${d.id}`];
            if (selected) {
                if (!gInput) {
                    var categories = [];
                    d.divisionGroups.map(dg => dg.genderCategories).flat().forEach(dc => {
                        if (!categories.some(c => c.id === dc.categoryId))
                            categories.push(dc.category);
                    });

                    inputs[`gender-${d.id}`] = {
                        type: 'select',
                        placeholder: `Select Gender (${d.name})`,
                        options: categories,
                        textField: 'name',
                        valueField: 'id',
                        rules: [{ required: true, message: 'Please select your gender' }],
                        onChange: this.handleGenderSelectedChanged,
                        division: d
                    };
                }
                if (!aInput) {
                    inputs[`age-${d.id}`] = {
                        type: 'select',
                        disabled: true,
                        placeholder: `Select Age (${d.name})`,
                        options: [],
                        textField: 'name',
                        valueField: 'id',
                        rules: [{ required: true, message: 'Please select your age' }],
                        onChange: this.handleAgeSelectedChanged,
                        division: d
                    };
                }
                if (!eInput) {
                    inputs[`experience-${d.id}`] = {
                        type: 'select',
                        disabled: true,
                        placeholder: `Select Experience Level (${d.name})`,
                        options: [],
                        textField: 'name',
                        valueField: 'id',
                        rules: [{ required: true, message: 'Please select your experience level' }],
                        onChange: this.handleExperienceSelectedChanged,
                        division: d
                    };
                }
                if (!wInput) {
                    inputs[`weight-${d.id}`] = {
                        type: 'select',
                        disabled: true,
                        placeholder: `Select Weight Category (${d.name})`,
                        options: [],
                        textField: 'name',
                        valueField: 'id',
                        rules: [{ required: true, message: 'Please select your weight category' }],
                        onChange: this.handleWeightSelectedChanged,
                        division: d
                    };
                }
            }
            else {
                delete inputs[`gender-${d.id}`];
                delete inputs[`age-${d.id}`];
                delete inputs[`experience-${d.id}`];
                delete inputs[`weight-${d.id}`];
            }
        });
        this.setState({ inputs: inputs });
    }

    handleGenderSelectedChanged = (e, input) => {
        let d = input.division;
        let categories = [];
        let divisionGroups = d.divisionGroups.filter(dg => dg.genderCategories.some(g => g.categoryId === e));
        divisionGroups.map(dg => dg.ageCategories).flat().forEach(dc => {
            if (!categories.some(c => c.id === dc.categoryId))
                categories.push(dc.category);
        });
        let inputsClone = { ...this.state.inputs };
        let inputClone = { ...inputsClone[`age-${d.id}`] };
        inputClone.options = categories.sort((a, b) => a.minAge - b.minAge);
        inputClone.disabled = categories.length === 0;
        inputClone.genderId = e;
        inputsClone[`age-${d.id}`] = inputClone;
        this.setState({ inputs: inputsClone });
    }

    handleAgeSelectedChanged = (e, input) => {
        let d = input.division;
        let categories = [];
        let divisionGroups = d.divisionGroups.filter(dg => dg.genderCategories.some(g => g.categoryId === input.genderId));
        divisionGroups.map(dg => dg.experienceCategories).flat().forEach(dc => {
            if (!categories.some(c => c.id === dc.categoryId))
                categories.push(dc.category);
        });
        let inputsClone = { ...this.state.inputs };
        let inputClone = { ...inputsClone[`experience-${d.id}`] };
        inputClone.options = categories.sort((a, b) => a.difficulty - b.difficulty);
        console.log(inputClone.options);
        inputClone.disabled = categories.length === 0;
        inputClone.genderId = input.genderId;
        inputClone.ageId = e;
        inputsClone[`experience-${d.id}`] = inputClone;
        this.setState({ inputs: inputsClone });
    }

    handleExperienceSelectedChanged = (e, input) => {
        let d = input.division;
        let categories = [];
        let divisionGroups = d.divisionGroups.filter(dg =>
            dg.genderCategories.some(g => g.categoryId === input.genderId) && dg.ageCategories.some(g => g.categoryId === input.ageId));

        divisionGroups.map(dg => dg.weightCategories).flat().forEach(dc => {
            if (!categories.some(c => c.id === dc.categoryId))
                categories.push(dc.category);
        });
        let inputsClone = { ...this.state.inputs };
        let inputClone = { ...inputsClone[`weight-${d.id}`] };
        inputClone.options = categories.sort((a, b) => a.minWeight - b.minWeight);
        inputClone.disabled = categories.length === 0;
        inputClone.experienceId = e;
        inputsClone[`weight-${d.id}`] = inputClone;
        this.setState({ inputs: inputsClone });
    }

    handleWeightSelectedChanged = (e, input) => {

    }

    handleSubmitCategoryChoice = (values) => {
        var registration = {
            registrationDivisions: values.divisions.map(d => {
                return {
                    divisionId: d,
                    genderCategoryId: values[`gender-${d}`],
                    ageCategoryId: values[`age-${d}`],
                    experienceCategoryId: values[`experience-${d}`],
                    weightCategoryId: values[`weight-${d}`],
                };
            })
        };
        this.props.onSubmit(registration);
    }

    render() {
        if (this.props.tournament) {
            return (
                <div>
                    <h4>Please choose your division.</h4>
                    <EditForm onSubmit={this.handleSubmitCategoryChoice} onCancel={null}
                        inputs={this.state.inputs} values={this.state.formValues} okText="Register" />

                </div>
            );
        }
        else
            return null;
    }
}

export default CategoryChoice;
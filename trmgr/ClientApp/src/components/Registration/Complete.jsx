﻿import React from 'react';
import './Complete.css';
import { Alert, Progress } from 'antd';

class Complete extends React.Component {
    render() {
        const resultReceived = this.props.registration || this.props.error;
        const progress = <Progress type="circle" percent={resultReceived ? 100 : 0} className="complete-icon" status={this.props.error ? "exception" : "success"} />;
        const message = resultReceived ? <Alert type={this.props.registration ? "success" : "error"} description={this.props.error ? this.props.error : "You are successfully registered."} /> : null;
            
        return (
            <div>
                {progress}
                {message}
            </div>
            );
        //if (this.props.registration) {
        //    return (
        //        <div>
        //            <Progress type="circle" percent={100}  className="complete-icon" />
                    
        //        </div>);
        //}
        //else {
        //    return (
        //        <div>
        //            <Progress type="circle" percent={100} status="exception" className="complete-icon" />
        //            <Alert type="error" description="Registration failed." />
        //        </div>);
        //}
    }
}

export default Complete;
﻿import React from 'react';
import { Tree } from 'antd';

const tree = (props) => {
    const checkedKeys = props.divisionCategories.map(c => c.categoryId);
    const groups = props.categoryGroups.map(ag => {
        const categories = ag.categories.map(ac => <Tree.TreeNode key={ac.id} title={ac.name} />);
        return <Tree.TreeNode key={`g-${ag.id}`} title={ag.name}>{categories}</Tree.TreeNode>;
    });
    return <Tree checkable onCheck={props.onCheck} checkedKeys={checkedKeys} defaultExpandAll>{groups}</Tree>;
};

export default tree;
﻿import React from 'react';
import { Form, Input, Button, DatePicker, Icon, Select, Checkbox, AutoComplete } from 'antd';
let id = 0;

class EditForm extends React.Component {
    state = {
        saving: false
    }

    remove = (k) => {
        const keys = this.props.form.getFieldValue('keys');
        this.props.form.setFieldsValue({ keys: keys.filter(key => key !== k) });
    }

    add = () => {
        const keys = this.props.form.getFieldValue('keys');
        const nextKeys = keys.concat(id++);
        this.props.form.setFieldsValue({ keys: nextKeys });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.setState({ loading: true });
                this.props.onSubmit(values);
            }
        });
    }
    render() {
        const { getFieldDecorator, getFieldValue  } = this.props.form;
        const inputs = Object.keys(this.props.inputs).map((i, idx) => {
            const input = this.props.inputs[i];
            let field;
            if (input.type === 'date') {
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values ? this.props.values[i] : null })(<DatePicker placeholder={input.placeholder} style={{ width: '100%' }} autoFocus={idx===0} />);
            }
            else if (input.type === 'select') {
                const options = input.options.map(o => <Select.Option key={o[input.valueField]} value={o[input.valueField]}>{o[input.textField]}</Select.Option>);
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values ? this.props.values[i] : undefined })(
                    <Select onChange={(e) => input.onChange(e, input)}
                        showSearch optionFilterProp="children" filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        placeholder={input.placeholder} disabled={input.disabled} style={{ width: '100%' }} autoFocus={idx === 0}>
                        {options}
                    </Select>);
            }
            else if (input.type === 'checkbox-group') {
                const options = input.options.map(o => <Checkbox key={o[input.valueField]} value={o[input.valueField]}>{o[input.textField]}</Checkbox>);
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values ? this.props.values[i] : null })(
                    <Checkbox.Group onChange={input.onChange}>
                        {options}
                    </Checkbox.Group>);
            }
            else if (input.type === 'auto-complete') {
                const options = input.options.map(o => o[input.textField]);
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values ? this.props.values[i] : null })(
                    <AutoComplete onSearch={input.onSearch} onSelect={input.onSelect} dataSource={options} placeholder={input.placeholder} style={{ width: '100%' }} />);
            }
            else {
                field = getFieldDecorator(i, { rules: input.rules, initialValue: this.props.values ? this.props.values[i] : null })(
                    <Input type={input.type} placeholder={input.placeholder} autoFocus={idx===0} step="any" />);
            }
            return (<Form.Item label={input.placeholder} key={i}>{field}</Form.Item>);
        });

        let formItems = null;
        let dynamicFieldButton = null;
        if (this.props.dynamicFieldProps) {
            getFieldDecorator('keys', { initialValue: [] });
            const keys = getFieldValue('keys');
            formItems = keys.map((k, index) => {
                const deleteButton = <Icon type="minus-circle-o" onClick={() => this.remove(k)} />
                return (<Form.Item required={false} key={k}>
                    {getFieldDecorator(`names[${k}]`, {
                        validateTrigger: ['onChange', 'onBlur'],
                        rules: this.props.dynamicFieldProps.rules
                    })(
                        <Input placeholder={this.props.dynamicFieldProps.placeholder} style={{ width: '100%' }} suffix={deleteButton} />
                    )}

                </Form.Item>);
            });

            dynamicFieldButton = (<Form.Item>
                <Button type="dashed" onClick={this.add} style={{ width: '100%' }}>
                    <Icon type="plus" /> Add Division
                    </Button>
            </Form.Item>);
        }

        
        return (
            <Form onSubmit={this.handleSubmit}>
                {inputs}
                {formItems}
                {dynamicFieldButton}
                <div className="form-buttons">
                    <Button className="form-button" onClick={this.props.onCancel}>Cancel</Button>
                    <Button className="form-button" type="primary" htmlType="submit" loading={this.state.loading}>{this.props.okText ? this.props.okText : 'Save'}</Button>
                </div>
            </Form>);
    }
}

export default Form.create({ name: 'edit-form' })(EditForm);
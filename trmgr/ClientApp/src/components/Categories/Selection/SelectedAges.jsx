﻿import React from 'react';
import RangeSelection from './RangeSelection';

const selected = (props) => {
    if (props.items && props.items.length > 0) {
        const min = Math.min(...props.items.map(i => i.category.minAge));
        const max = Math.max(...props.items.map(i => i.category.maxAge));
        return <RangeSelection title="Ages" min={min} max={max} />;
    }
    else {
        return <span>No Ages Selected</span>;
    }
};

export default selected;
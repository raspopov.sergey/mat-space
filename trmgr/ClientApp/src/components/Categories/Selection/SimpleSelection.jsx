﻿import React from 'react';

const selected = (props) => {
    const items = props.items ? props.items.map(i => i.category.name).join(' / ') : null;
    return (
        <div className="horizontal">
            <h4>{props.title}:&nbsp;</h4>
            <span>{items}</span>
        </div>
    );
};

export default selected;
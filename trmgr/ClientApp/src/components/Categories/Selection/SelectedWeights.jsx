﻿import React from 'react';
import RangeSelection from './RangeSelection';

const selected = (props) => {
    if (props.items && props.items.length > 0) {
        const min = Math.min(...props.items.map(i => i.category.minWeight));
        const max = Math.max(...props.items.map(i => i.category.maxWeight));
        return <RangeSelection title="Weights" min={min} max={max} />;
    }
    else {
        return <span>No Weights Selected</span>;
    }
};

export default selected;
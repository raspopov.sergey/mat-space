﻿import React from 'react';
import { Statistic } from 'antd';

const selected = (props) => {
    return (
        <div>
            <h4>{props.title}:</h4>
            <div className="horizontal">
                <Statistic title="From" value={props.min} style={{ marginRight: 20 }} />
                <Statistic title="To" value={props.max} />
            </div>
        </div>
    );
};

export default selected;
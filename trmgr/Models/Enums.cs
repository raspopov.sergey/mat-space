﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trmgr.Models
{
    public enum CategoryType
    {
        AGE,
        EXPERIENCE,
        GENDER,
        WEIGHT
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trmgr.Models.DatabaseModels.Organization.Categories.Base
{
    [Table("CategoryGroups")]
    public abstract class CategoryGroup
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }

        public List<Category> Categories { get; set; }

        public string ApplicationUserId { get; set; }
    }
}

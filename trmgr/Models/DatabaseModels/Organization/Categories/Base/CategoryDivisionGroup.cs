﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trmgr.Models.DatabaseModels.Organization.Categories.Base
{
    [Table("CategoryDivisionGroups")]
    public abstract class CategoryDivisionGroup
    {
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public int DivisionGroupId { get; set; }
        [JsonIgnore]
        public DivisionGroup DivisionGroup { get; set; }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;

namespace trmgr.Models.DatabaseModels.Organization.Categories
{
    public class ExperienceCategory : Category
    {
        public int Difficulty { get; set; }
    }

    public class ExperienceCategoryComparer : IComparer<Category>
    {
        public int Compare(Category x, Category y)
        {
            var c1 = x as ExperienceCategory;
            var c2 = y as ExperienceCategory;
            if (c1 == null || c2 == null)
                return 0;
            else
                return c1.Difficulty - c2.Difficulty;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;

namespace trmgr.Models.DatabaseModels.Organization.Categories
{
    public class AgeCategory : Category
    {
        [Range(0, 150)]
        public byte MaxAge { get; set; }
        [Range(0, 150)]
        public byte MinAge { get; set; }
    }

    public class AgeCategoryComparer : IComparer<Category>
    {
        public int Compare(Category x, Category y)
        {
            var ac1 = x as AgeCategory;
            var ac2 = y as AgeCategory;
            if (ac1 == null || ac2 == null)
                return 0;
            else
                return ac1.MinAge - ac2.MinAge;
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;

namespace trmgr.Models.DatabaseModels.Organization.Categories
{
    public class WeightCategory : Category
    {
        [Column(TypeName = "decimal(4,1)")]
        public decimal MinWeight { get; set; }
        [Column(TypeName = "decimal(4,1)")]
        public decimal MaxWeight { get; set; }
        public bool Absolute { get; set; }
    }

    public class WeightCategoryComparer : IComparer<Category>
    {
        public int Compare(Category x, Category y)
        {
            var c1 = x as WeightCategory;
            var c2 = y as WeightCategory;
            if (c1 == null || c2 == null)
                return 0;
            else
                return (int)c1.MinWeight - (int)c2.MinWeight;
        }
    }
}

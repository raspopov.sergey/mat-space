﻿using System;

namespace trmgr.Models.DatabaseModels.Organization
{
    public class Match
    {
        public int Id { get; set; }
        /// <summary>
        /// First round seed numbers are whole numbers. Next round will be decimal numbers like 0.1, 0.2. Next round will be 0.01, 0.02
        /// </summary>
        public double SeedNumber { get; set; }
        public Match WinnerMatch { get; set; }

        public byte Score1 { get; set; }
        public byte Advantages1 { get; set; }
        public byte Penalties1 { get; set; }
        public bool IsDQed1 { get; set; }

        public int? Registration1Id { get; set; }
        public Registration Registration1 { get; set; }

        public byte Score2 { get; set; }
        public byte Advantages2 { get; set; }
        public byte Penalties2 { get; set; }
        public bool IsDQed2 { get; set; }

        public int? Registration2Id { get; set; }
        public Registration Registration2 { get; set; }

        public int? WinnerId { get; set; }
        public Registration WinnerRegistration { get; set; }
        public int? LooserId { get; set; }
        public Registration LooserRegistration { get; set; }

        public int Round
        {
            get
            {
                var round = 1;
                var seedIterator = SeedNumber;
                while (seedIterator < 1)
                {
                    seedIterator *= 10;
                    round++;
                }

                return round;
            }
        }
    }

}

﻿using System.Collections.Generic;
using System.Linq;
using trmgr.Models.DatabaseModels.Organization.Categories;

namespace trmgr.Models.DatabaseModels.Organization
{
    public class Bracket
    {
        public int Id { get; set; }


        public int DivisionId { get; set; }
        public Division Division { get; set; }

        public int AgeCategoryId { get; set; }
        public AgeCategory AgeCategory { get; set; }

        public int GenderCategoryId { get; set; }
        public GenderCategory GenderCategory { get; set; }

        public int WeightCategoryId { get; set; }
        public WeightCategory WeightCategory { get; set; }

        public int ExperienceCategoryId { get; set; }
        public ExperienceCategory ExperienceCategory { get; set; }

        public ICollection<Match> Matches { get; set; }

        public Bracket()
        {
            Matches = new List<Match>();
        }

        public IEnumerable<Match> GetRoundMatches(int round)
        {
            return Matches.Where(m => m.Round == round);
        }
    }
}

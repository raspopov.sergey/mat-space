﻿using System.Collections.Generic;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;

namespace trmgr.Models.DatabaseModels.Organization
{
    public class DivisionGroup
    {
        public int Id { get; set; }
        public ICollection<AgeCategoryDivisionGroup> AgeCategories { get; set; }
        public ICollection<ExperienceCategoryDivisionGroup> ExperienceCategories { get; set; }
        public ICollection<WeightCategoryDivisionGroup> WeightCategories { get; set; }
        public ICollection<GenderCategoryDivisionGroup> GenderCategories { get; set; }

        public int DivisionId { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace trmgr.Models.DatabaseModels.Organization
{
    public class Division
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Bracket> Brackets { get; set; }
        public ICollection<DivisionGroup> DivisionGroups { get; set; }
        public ICollection<RegisteredDivision> Registrations { get; set; }

        public int TournamentId { get; set; }

        public Division()
        {
            Brackets = new List< Bracket>();
            DivisionGroups = new List<DivisionGroup>();
            Registrations = new List<RegisteredDivision>();
        }

        public void AddGroup(DivisionGroup group)
        {
            DivisionGroups.Add(group);
        }

        public override bool Equals(object obj)
        {
            return obj is Division division &&
                   Id == division.Id;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id);
        }
    }
}

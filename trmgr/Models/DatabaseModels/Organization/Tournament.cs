﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace trmgr.Models.DatabaseModels.Organization
{
    public class Tournament
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        
        public DateTime? Date { get; set; }
        public DateTime? RegistrationStart { get; set; }
        public DateTime? RegistrationEnd { get; set; }

        //divisions
        public ICollection<Division> Divisions { get; set; }
        public ICollection<Registration> Registrations { get; set; }

        public string ApplicationUserId { get; set; }

        public Tournament()
        {
            Divisions = new List<Division>();
            Registrations = new List<Registration>();
        }

        public void AddDivision(Division division)
        {
            Divisions.Add(division);
        }
    }
}
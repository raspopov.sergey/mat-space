﻿namespace trmgr.Models.DatabaseModels.School
{
    public class Affiliation
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

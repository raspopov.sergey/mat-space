﻿using System.Collections.Generic;

namespace trmgr.Models.DatabaseModels.School
{
    public class Club
    {
        public int Id { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Multiple names attributed to the same club
        /// </summary>
        public IEnumerable<ClubName> Names { get; set; }

        public int? CityId { get; set; }
        public City City { get; set; }

        public bool Confirmed { get; set; }
        public Affiliation Affiliation { get; set; }
    }
}

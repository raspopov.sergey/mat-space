﻿using System.ComponentModel.DataAnnotations;

namespace trmgr.Models.DatabaseModels.School
{
    public class ClubName
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}

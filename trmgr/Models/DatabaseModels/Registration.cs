﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;

namespace trmgr.Models.DatabaseModels
{
    public class Registration
    {
        public int Id { get; set; }
        public int TournamentId { get; set; }
        public Tournament Tournament { get; set; }

        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public string Club { get; set; }
        public ICollection<RegisteredDivision> RegistrationDivisions { get; set; }
    }

    public class RegisteredDivision
    {
        public int Id { get; set; }

        public int RegistrationId { get; set; }
        //[JsonIgnore]
        public Registration Registration { get; set; }

        public int DivisionId { get; set; }
        public Division Division { get; set; }

        public int AgeCategoryId { get; set; }
        public AgeCategory AgeCategory { get; set; }

        public int GenderCategoryId { get; set; }
        public GenderCategory GenderCategory { get; set; }

        public int ExperienceCategoryId { get; set; }
        public ExperienceCategory ExperienceCategory { get; set; }

        public int WeightCategoryId { get; set; }
        public WeightCategory WeightCategory { get; set; }
    }
}

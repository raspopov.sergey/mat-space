﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.School;

namespace trmgr.Models.DatabaseModels
{
    public class ApplicationUser : IdentityUser
    {
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public string StreetAddress { get; set; }
        public string Appartment { get; set; }
        public City City { get; set; }
        [Column(TypeName = "decimal(4,1)")]
        public decimal Weight { get; set; }
        public int Age { get; set; }
        public Club Club { get; set; }
        
        //organizer stuff
        public IEnumerable<Tournament> Tournaments { get; set; }
        /// <summary>
        /// store weigth category templates
        /// </summary>
        public ICollection<AgeCategoryGroup> AgeCategoryGroups { get; set; }
        public ICollection<ExperienceCategoryGroup> ExperienceCategoryGroups { get; set; }
        public ICollection<GenderCategoryGroup> GenderCategoryGroups { get; set; }
        public ICollection<WeightCategoryGroup> WeightCategoryGroups { get; set; }

    }
}

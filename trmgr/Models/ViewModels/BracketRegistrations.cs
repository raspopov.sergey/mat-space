﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;

namespace trmgr.Models.ViewModels
{
    public class BracketRegistrations
    {
        public Division Division { get; set; }

        public AgeCategory AgeCategory { get; set; }

        public GenderCategory GenderCategory { get; set; }

        public WeightCategory WeightCategory { get; set; }

        public ExperienceCategory ExperienceCategory { get; set; }
        public int BracketId { get; set; }
        public int RegistrationCount { get; set; }
    }
}

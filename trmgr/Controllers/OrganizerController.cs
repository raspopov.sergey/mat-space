﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models;
using trmgr.Models.DatabaseModels;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Services;

namespace trmgr.Controllers
{
    [Authorize(Roles = "Organizer")]
    [Route("api/[controller]")]
    public class OrganizerController : Controller
    {
        private OrganizerService _organizerService;
        private ILogger<OrganizerController> _logger;

        public OrganizerController(OrganizerService organizerService, ILogger<OrganizerController> logger)
        {
            _organizerService = organizerService;
            _logger = logger;
        }

        #region Category Group

        [HttpGet("[action]")]
        public async Task<IActionResult> Categories(CategoryType categoryType)
        {
            try
            {
                var userId = User.Identity.Name;
                var ageCategories = await _organizerService.GetCategoryGroupsAsync(userId, categoryType);
                return Ok(ageCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        #region Add Category Group

        [HttpPost("[action]")]
        public async Task<IActionResult> AddAgeCategoryGroup([FromBody] AgeCategoryGroup categoryGroup)
        {
            try
            {
                categoryGroup.ApplicationUserId = User.Identity.Name; ;
                await _organizerService.AddCategoryGroupAsync(categoryGroup);
                return Ok(categoryGroup);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddExperienceCategoryGroup([FromBody] ExperienceCategoryGroup categoryGroup)
        {
            try
            {
                categoryGroup.ApplicationUserId = User.Identity.Name; ;
                await _organizerService.AddCategoryGroupAsync(categoryGroup);
                return Ok(categoryGroup);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddGenderCategoryGroup([FromBody] GenderCategoryGroup categoryGroup)
        {
            try
            {
                categoryGroup.ApplicationUserId = User.Identity.Name; ;
                await _organizerService.AddCategoryGroupAsync(categoryGroup);
                return Ok(categoryGroup);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddWeightCategoryGroup([FromBody] WeightCategoryGroup categoryGroup)
        {
            try
            {
                categoryGroup.ApplicationUserId = User.Identity.Name; ;
                await _organizerService.AddCategoryGroupAsync(categoryGroup);
                return Ok(categoryGroup);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region Update Category Group

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateAgeCategoryGroup([FromBody] AgeCategoryGroup group)
        {

            try
            {
                await _organizerService.UpdateCategoryGroupAsync(CategoryType.AGE, group, User.Identity.Name);
                return Ok(group);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateExperienceCategoryGroup([FromBody] ExperienceCategoryGroup group)
        {
            try
            {
                await _organizerService.UpdateCategoryGroupAsync(CategoryType.EXPERIENCE, group, User.Identity.Name);
                return Ok(group);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateGenderCategoryGroup([FromBody] GenderCategoryGroup group)
        {
            try
            {
                await _organizerService.UpdateCategoryGroupAsync(CategoryType.GENDER, group, User.Identity.Name);
                return Ok(group);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateWeightCategoryGroup([FromBody] WeightCategoryGroup group)
        {
            try
            {
                await _organizerService.UpdateCategoryGroupAsync(CategoryType.WEIGHT, group, User.Identity.Name);
                return Ok(group);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion
        
        #region Delete Category Group

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteAgeCategoryGroup([FromBody] int groupId)
        {
            try
            {
                var deleted = await _organizerService.DeleteCategoryGroupAsync(CategoryType.AGE, groupId, User.Identity.Name);
                return Ok(deleted);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteExperienceCategoryGroup([FromBody] int groupId)
        {
            try
            {
                var deleted = await _organizerService.DeleteCategoryGroupAsync(CategoryType.EXPERIENCE, groupId, User.Identity.Name);
                return Ok(deleted);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteGenderCategoryGroup([FromBody] int groupId)
        {
            try
            {
                var deleted = await _organizerService.DeleteCategoryGroupAsync(CategoryType.GENDER, groupId, User.Identity.Name);
                return Ok(deleted);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteWeightCategoryGroup([FromBody] int groupId)
        {
            try
            {
                var deleted = await _organizerService.DeleteCategoryGroupAsync(CategoryType.WEIGHT, groupId, User.Identity.Name);
                return Ok(deleted);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #endregion

        #region Category

        #region Add Category

        [HttpPost("[action]")]
        public async Task<IActionResult> AddAgeCategory([FromBody] AgeCategory category)
        {
            try
            {
                var userId = User.Identity.Name;
                await _organizerService.AddCategoryAsync(CategoryType.AGE, category, userId);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddExperienceCategory([FromBody] ExperienceCategory category)
        {
            try
            {
                var userId = User.Identity.Name;
                await _organizerService.AddCategoryAsync(CategoryType.EXPERIENCE, category, userId);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddGenderCategory([FromBody] GenderCategory category)
        {
            try
            {
                var userId = User.Identity.Name;
                await _organizerService.AddCategoryAsync(CategoryType.GENDER, category, userId);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> AddWeightCategory([FromBody] WeightCategory category)
        {
            try
            {
                var userId = User.Identity.Name;
                await _organizerService.AddCategoryAsync(CategoryType.WEIGHT, category, userId);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion
        
        #region Update Category

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateAgeCategory([FromBody] AgeCategory category)
        {
            try
            {
                await _organizerService.UpdateCategoryAsync(CategoryType.AGE, category, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateExperienceCategory([FromBody] ExperienceCategory category)
        {
            try
            {
                await _organizerService.UpdateCategoryAsync(CategoryType.EXPERIENCE, category, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateGenderCategory([FromBody] GenderCategory category)
        {
            try
            {
                await _organizerService.UpdateCategoryAsync(CategoryType.GENDER, category, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("[action]")]
        public async Task<IActionResult> UpdateWeightCategory([FromBody] WeightCategory category)
        {
            try
            {
                await _organizerService.UpdateCategoryAsync(CategoryType.WEIGHT, category, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion
        
        #region Delete Category

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteAgeCategory([FromBody] int categoryId)
        {
            try
            {
                var category = await _organizerService.DeleteCategoryAsync(CategoryType.AGE, categoryId, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteExperienceCategory([FromBody] int categoryId)
        {
            try
            {
                var category = await _organizerService.DeleteCategoryAsync(CategoryType.EXPERIENCE, categoryId, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteGenderCategory([FromBody]  int categoryId)
        {
            try
            {
                var category = await _organizerService.DeleteCategoryAsync(CategoryType.GENDER, categoryId, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteWeightCategory([FromBody] int categoryId)
        {
            try
            {
                var category = await _organizerService.DeleteCategoryAsync(CategoryType.WEIGHT, categoryId, User.Identity.Name);
                return Ok(category);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #endregion
        
        [HttpGet("[action]")]
        public async Task<IActionResult> Tournaments()
        {
            try
            {
                var tournaments = await _organizerService.GetTournamentsAsync(User.Identity.Name);
                return Ok(tournaments);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Tournament([FromBody] Tournament tournament)
        {
            try
            {
                tournament = await _organizerService.AddTournamentAsync(tournament, User.Identity.Name);
                return Ok(tournament);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> Tournament([FromBody] int id)
        {
            try
            {
                var deleted = await _organizerService.DeleteTournamentAsync(id, User.Identity.Name);
                return Ok(deleted.Id);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("division")]
        public async Task<IActionResult> AddDivision([FromBody] Division division)
        {
            try
            {
                division = await _organizerService.AddDivisionAsync(division, User.Identity.Name);
                return Ok(division);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("[action]")]
        public async Task<IActionResult> DeleteDivision([FromBody] int divisionId)
        {
            try
            {
                var division = await _organizerService.DeleteDivisionAsync(divisionId, User.Identity.Name);
                return Ok(division);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DivisionGroup([FromBody] DivisionGroup divisionGroup)
        {
            try
            {
                divisionGroup = await _organizerService.AddDivisionGroupAsync(divisionGroup, User.Identity.Name);
                return Ok(divisionGroup);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("division-group")]
        public async Task<IActionResult> DeleteDivisionGroup([FromBody] int divisionGroupId)
        {
            try
            {
                var divisionGroup = await _organizerService.DeleteDivisionGroupAsync(divisionGroupId, User.Identity.Name);
                return Ok(divisionGroup);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Get Division Categories

        [HttpGet("[action]")]
        public async Task<IActionResult> DivisionAges(int divisionGroupId)
        {
            try
            {
                var divisionCategories = await _organizerService.GetDivisionAgesAsync(divisionGroupId, User.Identity.Name);
                return Ok(divisionCategories);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
        
        [HttpGet("[action]")]
        public async Task<IActionResult> DivisionExperiences(int divisionGroupId)
        {
            try
            {
                var divisionCategories = await _organizerService.GetDivisionExperiencesAsync(divisionGroupId, User.Identity.Name);
                return Ok(divisionCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> DivisionGenders(int divisionGroupId)
        {
            try
            {
                var divisionCategories = await _organizerService.GetDivisionGendersAsync(divisionGroupId, User.Identity.Name);
                return Ok(divisionCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> DivisionWeights(int divisionGroupId)
        {
            try
            {
                var divisionCategories = await _organizerService.GetDivisionWeightsAsync(divisionGroupId, User.Identity.Name);
                return Ok(divisionCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion

        #region Save Division Categories

        [HttpPost("[action]")]
        public async Task<IActionResult> DivisionAges([FromBody]DivisionGroup divisionGroup)
        {
            try
            {
                var division = await _organizerService.UpdateDivisionCategoriesAsync(CategoryType.AGE, divisionGroup, User.Identity.Name);
                return Ok(division.AgeCategories);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DivisionExperiences([FromBody]DivisionGroup divisionGroup)
        {
            try
            {
                var division = await _organizerService.UpdateDivisionCategoriesAsync(CategoryType.EXPERIENCE, divisionGroup, User.Identity.Name);
                return Ok(division.ExperienceCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DivisionGenders([FromBody]DivisionGroup divisionGroup)
        {
            try
            {
                var division = await _organizerService.UpdateDivisionCategoriesAsync(CategoryType.GENDER, divisionGroup, User.Identity.Name);
                return Ok(division.GenderCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> DivisionWeights([FromBody]DivisionGroup divisionGroup)
        {
            try
            {
                var division = await _organizerService.UpdateDivisionCategoriesAsync(CategoryType.WEIGHT, divisionGroup, User.Identity.Name);
                return Ok(division.WeightCategories);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        #endregion

        [HttpGet("brackets")]
        public async Task<IActionResult> GetBrackets([FromQuery]int tournamentId)
        {
            try
            {
                var tournament = await _organizerService.GetBracketsAsync(tournamentId, User.Identity.Name);
                return Ok(tournament);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}

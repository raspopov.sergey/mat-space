﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels;
using trmgr.Services;

namespace trmgr.Controllers
{
    [Route("api/[controller]")]
    public class AddressController : Controller
    {
        private AddressService _applicationService;

        public AddressController(AddressService applicationService)
        {
            _applicationService = applicationService;
        }
        
        [HttpGet("[action]")]
        public async Task<IActionResult> Countries()
        {
            try
            {
                var countries = await _applicationService.GetCountriesAsync();
                return Ok(countries);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        
        [HttpGet("[action]")]
        public async Task<IActionResult> Provinces([FromQuery] int countryId = 0, [FromQuery] int limit = 0)
        {
            try
            {
                var provinces = await _applicationService.GetProvincesAsync(countryId, limit);
                return Ok(provinces);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Cities([FromQuery] int provinceId, [FromQuery] string city = null, [FromQuery] int limit = 0)
        {
            try
            {
                var cities = await _applicationService.GetCitiesAsync(provinceId, city, limit);
                return Ok(cities);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> Clubs([FromQuery] int cityId = 0, [FromQuery] string club = null, [FromQuery] int limit = 0)
        {
            try
            {
                var clubs = await _applicationService.GetClubsAsync(cityId, club, limit);
                return Ok(clubs);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        [HttpGet("club-names")]
        public async Task<IActionResult> ClubNames([FromQuery] string club = null, [FromQuery] int limit = 0)
        {
            try
            {
                var clubNames = await _applicationService.GetClubNamesAsync(club, limit);
                return Ok(clubNames);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }
    }
}

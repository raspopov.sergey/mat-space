﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.Models.DatabaseModels;
using trmgr.Services;

namespace trmgr.Controllers
{
    [Route("api/[controller]")]
    public class RegistrationController : Controller
    {
        private RegistrationService _registrationService;
        private AddressService _addressService;

        public RegistrationController(RegistrationService registrationService, AddressService addressService)
        {
            _registrationService = registrationService;
            _addressService = addressService;
        }

        [HttpGet("available-tournaments")]
        public async Task<IActionResult> GetAvailableTournaments()
        {
            try
            {
                var tournaments = await _registrationService.GetAvailableTournaments();
                return Ok(tournaments);
            }
            catch (Exception ex)
            {
                return BadRequest("Something went wrong");
            }
        }

        [HttpGet("tournament")]
        public async Task<IActionResult> GetTournament([FromQuery]int tournamentId)
        {
            try
            {
                var tournament = await _registrationService.GetTournamentAsync(tournamentId);
                return Ok(tournament);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("registration")]
        public async Task<IActionResult> AddRegistration([FromBody] Registration registration)
        {
            try
            {
                await _registrationService.AddRegistrationAsync(registration);
                await _registrationService.AddRegistrationToBracketAsync(registration);
                return Ok(registration);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [HttpGet("division-group-registrations")]
        public async Task<IActionResult> GetDivisionGroupRegistrations([FromQuery]int divisionGroupId)
        {
            try
            {
                var registrationCounts = await _registrationService.GetBracketRegistrations(divisionGroupId);
                return Ok(registrationCounts);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }

        [HttpGet("bracket")]
        public async Task<IActionResult> GetBracket([FromQuery]int bracketId)
        {
            try
            {
                var bracket = await _registrationService.GetBracket(bracketId);
                bracket = await _registrationService.GenerateFullBracket(bracket);
                return Ok(bracket);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.GetBaseException().Message);
            }
        }
    }
}

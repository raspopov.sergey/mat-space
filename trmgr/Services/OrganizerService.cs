﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using trmgr.DAL;
using trmgr.Models;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.Organization.Categories.Base;
using trmgr.Models.DatabaseModels.Organization.Categories.ManyToMany;

namespace trmgr.Services
{
    public class OrganizerService
    {
        private ApplicationDbContext _context;

        public OrganizerService(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Category Group

        public async Task<IEnumerable<CategoryGroup>> GetCategoryGroupsAsync(string userId, CategoryType categoryType)
        {
            switch (categoryType)
            {
                case CategoryType.AGE:
                    var ages = await _context
                        .AgeCategoryGroups
                        .Where(acg => acg.ApplicationUserId == userId)
                        .Include(acg => acg.Categories)
                        .ToListAsync();
                    ages.ForEach(acg => acg.Categories.Sort(new AgeCategoryComparer()));
                    return ages;
                case CategoryType.EXPERIENCE:
                    var levels = await _context
                        .ExperienceCategoryGroups
                        .Where(acg => acg.ApplicationUserId == userId)
                        .Include(acg => acg.Categories)
                        .ToListAsync();
                    levels.ForEach(g => g.Categories.Sort(new ExperienceCategoryComparer()));
                    return levels;
                case CategoryType.GENDER:
                    return await _context
                        .GenderCategoryGroups
                        .Where(acg => acg.ApplicationUserId == userId)
                        .Include(acg => acg.Categories)
                        .ToListAsync();
                case CategoryType.WEIGHT:
                    var weights = await _context
                        .WeightCategoryGroups
                        .Where(acg => acg.ApplicationUserId == userId)
                        .Include(acg => acg.Categories)
                        .ToListAsync();
                    weights.ForEach(g => g.Categories.Sort(new WeightCategoryComparer()));
                    return weights;
                default:
                    throw new Exception($"Unknown category type {categoryType}");
            }
        }

        public async Task<CategoryGroup> AddCategoryGroupAsync(CategoryGroup categoryGroup)
        {
            _context.Add(categoryGroup);
            await _context.SaveChangesAsync();
            return categoryGroup;
        }

        public async Task<CategoryGroup> UpdateCategoryGroupAsync(CategoryType categoryType, CategoryGroup group, string userId)
        {
            if (await VerifyCategoryGroupAsync(categoryType, group.Id, userId))
            {
                group.ApplicationUserId = userId;
                _context.Update(group);
                await _context.SaveChangesAsync();
                return group;
            }
            throw new Exception("Something went wrong!");

        }

        public async Task<CategoryGroup> DeleteCategoryGroupAsync(CategoryType categoryType, int groupId, string userId)
        {
            var group = await GetCategoryGroupAsync(categoryType, groupId, userId);
            var entity = _context.Remove(group);
            await _context.SaveChangesAsync();
            return entity.Entity;
        }


        #endregion

        #region Category

        public async Task<Category> AddCategoryAsync(CategoryType categoryType, Category category, string userId)
        {
            var group = await GetCategoryGroupAsync(categoryType, category.CategoryGroupId, userId);
            if (group != null)
            {
                _context.Add(category);
                await _context.SaveChangesAsync();
                return category;
            }
            throw new Exception("Could not add a category");//this should never happen
        }

        public async Task<Category> UpdateCategoryAsync(CategoryType categoryType, Category category, string userId)
        {
            if (await GetCategoryGroupAsync(categoryType, category.CategoryGroupId, userId) != null)
            {
                _context.Update(category);
                await _context.SaveChangesAsync();
                return category;
            }
            throw new Exception("Something went wrong");
        }

        public async Task<Category> DeleteCategoryAsync(CategoryType categoryType, int categoryId, string userId)
        {
            var category = await GetCategoryAsync(categoryType, categoryId);
            if (category != null && category.CategoryGroup.ApplicationUserId == userId)
            {
                _context.Remove(category);
                await _context.SaveChangesAsync();
                return category;
            }
            throw new Exception("Something went wrong");
        }

        #endregion

        #region Get Category Group

        private async Task<CategoryGroup> GetCategoryGroupAsync(CategoryType categoryType, int groupId, string userId)
        {
            switch (categoryType)
            {
                case CategoryType.AGE:
                    return await _context.AgeCategoryGroups.FirstOrDefaultAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.EXPERIENCE:
                    return await _context.ExperienceCategoryGroups.FirstOrDefaultAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.GENDER:
                    return await _context.GenderCategoryGroups.FirstOrDefaultAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.WEIGHT:
                    return await _context.WeightCategoryGroups.FirstOrDefaultAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                default:
                    throw new Exception($"Incorrect category type");
            }   
        }

        private async Task<bool> VerifyCategoryGroupAsync(CategoryType categoryType, int groupId, string userId)
        {
            switch (categoryType)
            {
                case CategoryType.AGE:
                    return await _context.AgeCategoryGroups.AnyAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.EXPERIENCE:
                    return await _context.ExperienceCategoryGroups.AnyAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.GENDER:
                    return await _context.GenderCategoryGroups.AnyAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                case CategoryType.WEIGHT:
                    return await _context.WeightCategoryGroups.AnyAsync(g => g.ApplicationUserId == userId && g.Id == groupId);
                default:
                    throw new Exception($"Incorrect category type");
            }
        }

        private async Task<Category> GetCategoryAsync(CategoryType categoryType, int categoryId)
        {
            switch (categoryType)
            {
                case CategoryType.AGE:
                    return await _context.AgeCategories.Include(c => c.CategoryGroup).FirstOrDefaultAsync(c => c.Id == categoryId);
                case CategoryType.EXPERIENCE:
                    return await _context.ExperienceCategories.Include(c => c.CategoryGroup).FirstOrDefaultAsync(c => c.Id == categoryId);
                case CategoryType.GENDER:
                    return await _context.GenderCaegories.Include(c => c.CategoryGroup).FirstOrDefaultAsync(c => c.Id == categoryId);
                case CategoryType.WEIGHT:
                    return await _context.WeightCategories.Include(c => c.CategoryGroup).FirstOrDefaultAsync(c => c.Id == categoryId);
                default:
                    throw new Exception($"Incorrect category type");
            }
        }

        #endregion


        public async Task<IEnumerable<Tournament>> GetTournamentsAsync(string userId)
        {
            var tournaments = await _context
                .Tournaments
                .Where(t => t.ApplicationUserId == userId)
                .Include(t => t.Divisions).ToListAsync();
            return tournaments;
        }
        
        public async Task<Tournament> AddTournamentAsync(Tournament tournament, string userId)
        {
            tournament.ApplicationUserId = userId;
            _context.Tournaments.Add(tournament);
            await _context.SaveChangesAsync();
            return tournament;
        }
        
        public async Task<Tournament> DeleteTournamentAsync(int id, string userId)
        {
            var existing = await GetTournamentAsync(id, userId);
            var entry = _context.Remove(existing);
            await _context.SaveChangesAsync();
            return entry.Entity;
        }

        public async Task<DivisionGroup> AddDivisionGroupAsync(DivisionGroup divisionGroup, string userId)
        {
            var division = await GetDivisionAsync(divisionGroup.DivisionId, userId);
            division.AddGroup(divisionGroup);
            _context.Update(division);
            await _context.SaveChangesAsync();
            return divisionGroup;
        }

        public async Task<DivisionGroup> DeleteDivisionGroupAsync(int divisionGroupId, string userId)
        {
            var divisionGroup = await GetDivisionGroupAsync(divisionGroupId, userId);
            _context.Remove(divisionGroup);
            await _context.SaveChangesAsync();
            return divisionGroup;
        }

        public async Task<Division> AddDivisionAsync(Division division, string userId)
        {
            var tournament = await GetTournamentAsync(division.TournamentId, userId);
            tournament.AddDivision(division);
            _context.Update(tournament);
            await _context.SaveChangesAsync();
            return division;
        }

        public async Task<Division> DeleteDivisionAsync(int divisionId, string userId)
        {
            var division = await GetDivisionAsync(divisionId, userId);
            _context.Remove(division);
            await _context.SaveChangesAsync();
            return division;
        }

        #region Division Category Handling

        public async Task<IEnumerable<AgeCategoryDivisionGroup>> GetDivisionAgesAsync(int divisionGroupId, string userId)
        {
            var query = from dg in _context.DivisionGroups
                        join d in _context.Divisions on dg.DivisionId equals d.Id
                        join t in _context.Tournaments on d.TournamentId equals t.Id
                        where dg.Id == divisionGroupId && t.ApplicationUserId == userId
                        select dg.AgeCategories;

            return await query.SelectMany(c => c).ToListAsync();
        }

        public async Task<IEnumerable<ExperienceCategoryDivisionGroup>> GetDivisionExperiencesAsync(int divisionGroupId, string userId)
        {
            var query = from dg in _context.DivisionGroups
                        join d in _context.Divisions on dg.DivisionId equals d.Id
                        join t in _context.Tournaments on d.TournamentId equals t.Id
                        where dg.Id == divisionGroupId && t.ApplicationUserId == userId
                        select dg.ExperienceCategories;

            return await query.SelectMany(c => c).ToListAsync();
        }

        public async Task<IEnumerable<GenderCategoryDivisionGroup>> GetDivisionGendersAsync(int divisionGroupId, string userId)
        {
            var query = from dg in _context.DivisionGroups
                        join d in _context.Divisions on dg.DivisionId equals d.Id
                        join t in _context.Tournaments on d.TournamentId equals t.Id
                        where dg.Id == divisionGroupId && t.ApplicationUserId == userId
                        select dg.GenderCategories;

            return await query.SelectMany(c => c).ToListAsync();
        }

        public async Task<IEnumerable<WeightCategoryDivisionGroup>> GetDivisionWeightsAsync(int divisionGroupId, string userId)
        {
            var query = from dg in _context.DivisionGroups
                        join d in _context.Divisions on dg.DivisionId equals d.Id
                        join t in _context.Tournaments on d.TournamentId equals t.Id
                        where dg.Id == divisionGroupId && t.ApplicationUserId == userId
                        select dg.WeightCategories;

            return await query.SelectMany(c => c).ToListAsync();
        }


        public async Task<DivisionGroup> UpdateDivisionCategoriesAsync(CategoryType categoryType, DivisionGroup divisionGroup, string userId)
        {
            var storedDivisionGroup = await GetDivisionGroupAsync(divisionGroup.Id, userId);
            switch (categoryType)
            {
                case CategoryType.AGE:
                    storedDivisionGroup.AgeCategories = divisionGroup.AgeCategories;
                    break;
                case CategoryType.EXPERIENCE:
                    storedDivisionGroup.ExperienceCategories = divisionGroup.ExperienceCategories;
                    break;
                case CategoryType.GENDER:
                    storedDivisionGroup.GenderCategories = divisionGroup.GenderCategories;
                    break;
                case CategoryType.WEIGHT:
                    storedDivisionGroup.WeightCategories = divisionGroup.WeightCategories;
                    break;
            }
            _context.Update(storedDivisionGroup);
            await _context.SaveChangesAsync();
            return storedDivisionGroup;
        }

        #endregion

        private async Task<Tournament> GetTournamentAsync(int id, string userId)
        {
            return await _context.Tournaments
                .FirstOrDefaultAsync(t => t.Id == id && t.ApplicationUserId == userId);
        }


        private async Task<DivisionGroup> GetDivisionGroupAsync(int divisionGroupId, string userId)
        {
            var query = from dg in _context.DivisionGroups
                        join d in _context.Divisions on dg.DivisionId equals d.Id
                        join t in _context.Tournaments on d.TournamentId equals t.Id
                        where dg.Id == divisionGroupId && t.ApplicationUserId == userId
                        select dg;
            return await query
                .Include(dg => dg.AgeCategories)
                .Include(dg => dg.ExperienceCategories)
                .Include(dg => dg.GenderCategories)
                .Include(dg => dg.WeightCategories)
                .FirstOrDefaultAsync();
        }

        private async Task<Division> GetDivisionAsync(int divisionId, string userId)
        {
            var query = from t in _context.Tournaments
                        join d in _context.Divisions on t.Id equals d.TournamentId
                        where t.ApplicationUserId == userId && d.Id == divisionId
                        select d;
            return await query.FirstOrDefaultAsync();
        }

        public async Task<Tournament> GetBracketsAsync(int tournamentId, string userId)
        {
            return await _context.Tournaments
                .Include(t => t.Divisions)
                .ThenInclude(d => d.Registrations)
                .ThenInclude(r => r.Registration)
                .FirstOrDefaultAsync(t => t.Id == tournamentId && t.ApplicationUserId == userId);
        }
    }
}

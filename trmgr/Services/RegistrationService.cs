﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.DAL;
using trmgr.Models.DatabaseModels;
using trmgr.Models.DatabaseModels.Organization;
using trmgr.Models.DatabaseModels.Organization.Categories;
using trmgr.Models.DatabaseModels.School;
using trmgr.Models.ViewModels;

namespace trmgr.Services
{
    public class RegistrationService
    {
        private ApplicationDbContext _context;

        public RegistrationService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Tournament>> GetAvailableTournaments()
        {
            return await _context
                .Tournaments
                .Where(t => t.Date.HasValue && t.Date.Value >= DateTime.Today)
                .ToListAsync();
        }

        public async Task<Tournament> GetTournamentAsync(int id)
        {
            var query = _context.Tournaments
                .Include(t => t.Divisions).ThenInclude(d => d.DivisionGroups).ThenInclude(dg => dg.AgeCategories).ThenInclude(x => x.Category)
                .Include(t => t.Divisions).ThenInclude(d => d.DivisionGroups).ThenInclude(dg => dg.ExperienceCategories).ThenInclude(x => x.Category)
                .Include(t => t.Divisions).ThenInclude(d => d.DivisionGroups).ThenInclude(dg => dg.GenderCategories).ThenInclude(x => x.Category)
                .Include(t => t.Divisions).ThenInclude(d => d.DivisionGroups).ThenInclude(dg => dg.WeightCategories).ThenInclude(x => x.Category);

            var tournament = await query.FirstOrDefaultAsync(t => t.Id == id);
            return tournament;
        }

        public async Task<IEnumerable<BracketRegistrations>> GetBracketRegistrations(int divisionGroupId)
        {
            var q = from rd in _context.RegisteredDivisions.Include(d => d.Division)
                    join d in _context.Divisions on rd.DivisionId equals d.Id
                    join dg in _context.DivisionGroups on d.Id equals dg.DivisionId
                    join adg in _context.AgeCategoryDivisionGroups on rd.AgeCategoryId equals adg.CategoryId
                    join edg in _context.ExperienceCategoryDivisionGroups on rd.ExperienceCategoryId equals edg.CategoryId
                    join gdg in _context.GenderCategoryDivisionGroups on rd.GenderCategoryId equals gdg.CategoryId
                    join wdg in _context.WeightCategoryDivisionGroups on rd.WeightCategoryId equals wdg.CategoryId
                    join b in _context.Brackets on new { rd.DivisionId, rd.AgeCategoryId, rd.ExperienceCategoryId, rd.GenderCategoryId, rd.WeightCategoryId } equals new { b.DivisionId, b.AgeCategoryId, b.ExperienceCategoryId, b.GenderCategoryId, b.WeightCategoryId }
                    where adg.DivisionGroupId == divisionGroupId
                    where edg.DivisionGroupId == divisionGroupId
                    where gdg.DivisionGroupId == divisionGroupId
                    where wdg.DivisionGroupId == divisionGroupId
                    where dg.Id == divisionGroupId
                    group rd by new 
                    {
                        rd.Division,
                        rd.AgeCategory,
                        rd.ExperienceCategory,
                        rd.GenderCategory,
                        rd.WeightCategory,
                        b.Id
                    } into g
                    select new BracketRegistrations()
                    {
                        Division = g.Key.Division,
                        AgeCategory = g.Key.AgeCategory,
                        ExperienceCategory = g.Key.ExperienceCategory,
                        GenderCategory = g.Key.GenderCategory,
                        WeightCategory = g.Key.WeightCategory,
                        BracketId = g.Key.Id,
                        RegistrationCount = g.Count()
                    };

            var r = await q.ToListAsync();
            return r;
        }

        public async Task<Bracket> GetBracket(int bracketId)
        {
            var bracket = await _context
                .Brackets.Where(b => b.Id == bracketId)
                .Include(b => b.Matches)
                .ThenInclude(c => c.Registration1)
                .Include(b => b.Matches)
                .ThenInclude(c => c.Registration2)
                .FirstOrDefaultAsync(b => b.Id == bracketId);
            return bracket;
        }

        public async Task<Bracket> GenerateFullBracket(Bracket bracket)
        {
            var matches = bracket.Matches;
            var round = 1;
            var roundMatches = bracket.GetRoundMatches(round);
            while (roundMatches.Count() > 1)
            {
                var seedStep = Math.Pow(10, 1 - round);
                var start = seedStep;
                foreach(var match in roundMatches.OrderBy(m => m.SeedNumber))
                {
                    if (match.WinnerMatch == null) //only assign winner match if one doesn't exist already
                    {
                        var nextSeedNumber = match.SeedNumber + seedStep;
                        match.WinnerMatch = new Match() { SeedNumber = nextSeedNumber / 20 };
                        var closestMatch = roundMatches.FirstOrDefault(m => m.SeedNumber == (nextSeedNumber));
                        if (closestMatch != null)
                        {
                            if (closestMatch.WinnerMatch == null)
                            {
                                closestMatch.WinnerMatch = match.WinnerMatch;
                            }
                        }
                        bracket.Matches.Add(match.WinnerMatch);
                    }
                }

                round++;
                roundMatches = bracket.GetRoundMatches(round);
            }

            await _context.SaveChangesAsync();
            return bracket;
        }

        public async Task<Registration> AddRegistrationAsync(Registration registration)
        {
            if (!string.IsNullOrWhiteSpace(registration.Club))
            {
                var clubName = await _context.ClubNames.FirstOrDefaultAsync(c => c.Name.Equals(registration.Club, StringComparison.InvariantCultureIgnoreCase));
                if (clubName == null)
                    _context.Add(new ClubName() { Name = registration.Club });
            }
            _context.Registrations.Add(registration);
            await _context.SaveChangesAsync();
            return registration;
        }

        public async Task AddRegistrationToBracketAsync(Registration reg)
        {
            foreach(var rd in reg.RegistrationDivisions)
            {
                var bracket = await _context.Brackets
                    .Include(b => b.Matches)
                    .FirstOrDefaultAsync(b => b.DivisionId == rd.DivisionId && b.AgeCategoryId == rd.AgeCategoryId && b.ExperienceCategoryId == rd.ExperienceCategoryId && b.GenderCategoryId == rd.GenderCategoryId && b.WeightCategoryId == rd.WeightCategoryId);

                var rds = _context
                    .RegisteredDivisions
                    .Where(r => r.DivisionId == rd.DivisionId && r.AgeCategoryId == rd.AgeCategoryId && r.ExperienceCategoryId == rd.ExperienceCategoryId && r.GenderCategoryId == rd.GenderCategoryId && r.WeightCategoryId == rd.WeightCategoryId)
                    .Include(r => r.Registration)
                    .ToList();

                if (bracket == null)
                    bracket =_context.Brackets
                        .Add(new Bracket() { DivisionId = rd.DivisionId, AgeCategoryId = rd.AgeCategoryId, ExperienceCategoryId = rd.ExperienceCategoryId, GenderCategoryId = rd.GenderCategoryId, WeightCategoryId = rd.WeightCategoryId })
                        .Entity;
                
                var shuffled = Shuffle(rds);

                var matches = bracket.Matches;
                
                foreach (var match in matches)
                {
                    match.Registration1Id = null;
                    match.Registration2Id = null;
                }
                
                foreach(var key in shuffled.Keys)
                {
                    var seedNumber = key / 2 + 1;
                    var match = matches.FirstOrDefault(m => m.SeedNumber == seedNumber);
                    if (match == null)
                    {
                        match = new Match() { SeedNumber = seedNumber };
                        matches.Add(match);
                    }

                    if (match.Registration1Id == null)
                        match.Registration1Id = shuffled[key].RegistrationId;
                    else if (match.Registration2Id == null)
                        match.Registration2Id = shuffled[key].RegistrationId;
                }

                await _context.SaveChangesAsync();
            }
        }

        private Dictionary<int, RegisteredDivision> Shuffle(List<RegisteredDivision> regs)
        {
            var res = new Dictionary<int, RegisteredDivision>();
            var total = regs.Count;
            var fullBracketTotal = Convert.ToInt32(Math.Pow(2, Math.Ceiling(Math.Log(total, 2)))); //max amount of regs at this point 2, 4, 6, 8, 16 etc
            var grouped = regs.GroupBy(rd => rd.Registration.Club).OrderByDescending(g => g.Count());
            foreach(var group in grouped)
            {
                var i = 0;
                var step = fullBracketTotal / 2;
                var takenByCurrent = new Dictionary<int, RegisteredDivision>();//spots taken by currently processed group
                foreach (var r in group)
                {
                    while (takenByCurrent.ContainsKey(i)) { i += step; };
                    var j = i;
                    while (res.ContainsKey(j)) { j++; }

                    res.Add(j, r);
                    takenByCurrent.Add(j, r);

                    i += step;
                    if (i >= total)
                    {
                        i = 0;
                        step = step / 2;
                    }
                }
            }
            return res;
        }

    }
}

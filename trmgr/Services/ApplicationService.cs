﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using trmgr.DAL;
using trmgr.Models.DatabaseModels;
using trmgr.Models.DatabaseModels.School;

namespace trmgr.Services
{
    public class AddressService
    {
        private ApplicationDbContext _context;

        public AddressService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Country>> GetCountriesAsync()
        {
            return await _context.Countries.ToListAsync();
        }
        
        public async Task<IEnumerable<Province>> GetProvincesAsync(int countryId, int limit)
        {
            var query = _context.Provinces.AsQueryable();
            if (countryId > 0)
                query = query.Where(p => p.Country.Id == countryId);
            if (limit > 0)
                query = query.Take(limit);
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<City>> GetCitiesAsync(int provinceId, string city, int limit)
        {
            var query = _context.Cities.AsQueryable();
            if (provinceId > 0)
                query = query.Where(c => c.ProvinceId == provinceId);
            if (!string.IsNullOrWhiteSpace(city))
                query = query.Where(c => c.Name.StartsWith(city, StringComparison.CurrentCultureIgnoreCase));
            if (limit > 0)
                query = query.Take(limit);
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<Club>> GetClubsAsync(int cityId, string club, int limit)
        {
            var query = _context.Clubs.AsQueryable();
            if (cityId > 0)
                query = query.Where(c => c.CityId == cityId);
            if (!string.IsNullOrWhiteSpace(club))
                query = query.Where(c => c.Name.StartsWith(club, StringComparison.InvariantCultureIgnoreCase));
            if (limit > 0)
                query = query.Take(limit);
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<ClubName>> GetClubNamesAsync(string club, int limit)
        {
            var query = _context.ClubNames.AsQueryable();
            if (!string.IsNullOrWhiteSpace(club))
                query = query.Where(c => c.Name.StartsWith(club, StringComparison.InvariantCultureIgnoreCase));
            if (limit > 0)
                query = query.Take(limit);
             return await query.ToListAsync();
        }

        public async Task<Club> GetClubAsync(string clubName, int cityId)
        {
            return await _context.Clubs.FirstOrDefaultAsync(c => c.Name.Equals(clubName, StringComparison.CurrentCultureIgnoreCase) && c.CityId == cityId);
        }
    }
}
